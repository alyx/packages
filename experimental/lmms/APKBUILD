# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=lmms
pkgver=1.2.0
pkgrel=0
pkgdesc="Music production software"
url="https://lmms.io/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="alsa-lib-dev cmake fftw-dev fltk-dev fluidsynth-dev lame-dev
	libogg-dev libsamplerate-dev libsndfile-dev libvorbis-dev libxml2-dev
	pulseaudio-dev qt5-qtbase-dev qt5-qttools-dev qt5-qtx11extras-dev
	sdl-dev extra-cmake-modules xcb-util-dev xcb-util-keysyms-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="lmms-${pkgver/_/-}.tar.gz::https://github.com/LMMS/lmms/archive/v${pkgver/_/-}.tar.gz
	https://distfiles.adelielinux.org/source/qt5-x11embed-20171106.tar.xz
	rpmalloc-1.3.1.tar.gz::https://github.com/rampantpixels/rpmalloc/archive/1.3.1.tar.gz
	thread.patch
	use-system-ecm.patch
	"
builddir="$srcdir"/lmms-${pkgver/_/-}

prepare() {
	cd "$builddir"
	rmdir src/3rdparty/qt5-x11embed
	rmdir src/3rdparty/rpmalloc/rpmalloc
	mv "$srcdir"/qt5-x11embed-20171106 src/3rdparty/qt5-x11embed
	mv "$srcdir"/rpmalloc-1.3.1 src/3rdparty/rpmalloc/rpmalloc
	default_prepare
}

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DWANT_QT5=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="55b338b68013645175f79fb8e68278cc31defdc04e6c069b4f9adbdb6f8aaf52a60087bbbef56caeceac179e19754b1fd341df201adee4b1ff90de6bbd49ef22  lmms-1.2.0.tar.gz
d00b5dfb2931ae1614b419b06f795c5627e51eb2159612eb058484c2b1c6817532d4e321ce514518ce2009d8c7f886e384d6cc8b8428b05b268db3f28bb90e63  qt5-x11embed-20171106.tar.xz
c9a42c635774285a8467eaa1931109528c08931c73873c366d0e4949921c8956a31db7422378cd548bbbaf24d0fcc41470ab6751e67238db53677832e5fb5db4  rpmalloc-1.3.1.tar.gz
e6b5b87af969bf8c1c24cef2748485d86d58139704efdebf0645c376c9c40de54b0ff08d9fd3b41865670539c7dac50e4178d06dfb779e1d8d4fc7cdd08c4a32  thread.patch
70f1467340f7f5c9e94af702e036aa3355c036b13523a9d5183d5b0fb7c18f8878ec92c6d84a49837739b207bd0546ef5264a4e17f30ddf27b6f8eb9d09c4c76  use-system-ecm.patch"
