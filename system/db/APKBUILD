# Maintainer: 
pkgname=db
pkgver=5.3.28
pkgrel=1
pkgdesc="The Berkeley DB embedded database system"
url="https://www.oracle.com/technology/software/products/berkeley-db/index.html"
arch="all"
license="Sleepycat AND BSD-3-Clause"
options="!check"  # "check target not available"
depends=
makedepends=
subpackages="$pkgname-dev $pkgname-doc $pkgname-utils $pkgname-c++:cxx"
source="https://download.oracle.com/berkeley-db/db-$pkgver.tar.gz
	atomics.patch
	"

prepare() {
	cd "$builddir"
	update_config_sub
	default_prepare
}

build () {
	cd "$builddir"/build_unix
	../dist/configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--enable-compat185 \
		--enable-shared \
		--enable-cxx \
		--disable-static
	make LIBSO_LIBS=-lpthread
}

package() {
	cd "$builddir"/build_unix
	make DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/usr/share/doc
	mv "$pkgdir"/usr/docs "$pkgdir"/usr/share/doc/$pkgname

	install -D -m644 "$builddir"/LICENSE \
		"$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

utils() {
	pkgdesc="Utils for The Berkeley DB embedded database system"
	replaces="db"
	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

cxx() {
	pkgdesc="C++ binding for libdb"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libdb_cxx*.so "$subpkgdir"/usr/lib/
}

sha512sums="e91bbe550fc147a8be7e69ade86fdb7066453814971b2b0223f7d17712bd029a8eff5b2b6b238042ff6ec1ffa6879d44cb95c5645a922fee305c26c3eeaee090  db-5.3.28.tar.gz
8ba96cfc3e484a839b8651214f0769b35273c6235de7e53d5118eb0347f5a477f75e3336a12f1399b7748c6b6ab95aec39c8b813d7b227dd61f37ed4ab52f7d5  atomics.patch"
