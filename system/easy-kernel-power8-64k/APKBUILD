# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: Adelie Platform Group <adelie-devel@lists.adelielinux.org>
# KEEP THIS IN SYNC with the other easy-kernel packages.
_kflavour=-power8-64k
_pkgname=easy-kernel$_kflavour
pkgver=4.14.127
pkgrel=14
pkgname=$_pkgname-$pkgver-mc$pkgrel
pkgdesc="The Linux kernel, packaged for POWER8 and POWER9 computers (with 64K page size)"
url="https://kernel.org/"
arch="ppc64"
options="!check !dbg !strip !tracedeps"
license="GPL-2.0-only"
depends=""
makedepends="bc gzip kmod lzop openssl-dev xz"
provides="easy-kernel$_kflavour=$pkgver-r$pkgrel"
subpackages="$_pkgname-modules-$pkgver-mc$pkgrel:modules
	$_pkgname-src-$pkgver-mc$pkgrel:src
	"
_pkgmajver=${pkgver%%.*}
_pkgminver=${pkgver%.*}
source="https://cdn.kernel.org/pub/linux/kernel/v${_pkgmajver}.x/linux-${_pkgminver}.tar.xz
	https://distfiles.adelielinux.org/source/linux-${_pkgminver}-mc$pkgrel.patch.xz
	ast-endianness.patch
	config-ppc64
	"
builddir="$srcdir/linux-${_pkgminver}"

prepare() {
	cd "$srcdir"
	cat linux-${_pkgminver}-mc$pkgrel.patch.xz | unxz -> linux-${_pkgminver}-mc$pkgrel.patch
	patch -Np1 -d "$builddir" <linux-${_pkgminver}-mc$pkgrel.patch
	default_prepare
	cd "$srcdir"
	cp config-$CARCH linux-${_pkgminver}/.config
	cp -pr linux-${_pkgminver} linux-src
}

build() {
	cd "$builddir"
	make LDFLAGS=""

	cd "$srcdir/linux-src"
	make LDFLAGS="" modules_prepare clean
	cp "$builddir/Module.symvers" .
}

package() {
	cd "$builddir"
	mkdir -p "$pkgdir"/boot
	make INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		install modules_install

	if [ -f "$pkgdir"/boot/vmlinuz ]; then
		mv "$pkgdir"/boot/vmlinuz \
			"$pkgdir"/boot/vmlinuz-$pkgver-mc$pkgrel-easy$_kflavour
	fi
	if [ -f "$pkgdir"/boot/vmlinux ]; then
		mv "$pkgdir"/boot/vmlinux \
			"$pkgdir"/boot/vmlinux-$pkgver-mc$pkgrel-easy$_kflavour
	fi

	mv "$pkgdir"/boot/System.map "$pkgdir"/boot/System.map-$pkgver-mc$pkgrel-easy$_kflavour

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/easy-$pkgver-mc$pkgrel$_kflavour/kernel.release
}

modules() {
	pkgdesc="Modules / device drivers for easy-kernel"
	provides="easy-kernel$_kflavour-modules=$pkgver-r$pkgrel"
	autodeps=0  # modules should not depend on src just for symlink
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/modules "$subpkgdir"/lib/
	rm "$subpkgdir"/lib/modules/$pkgver-mc$pkgrel-easy$_kflavour/build
	rm "$subpkgdir"/lib/modules/$pkgver-mc$pkgrel-easy$_kflavour/source
	ln -s "../../../usr/src/linux-$pkgver-mc$pkgrel$_kflavour" \
		"$subpkgdir"/lib/modules/$pkgver-mc$pkgrel-easy$_kflavour/build
	ln -s "../../../usr/src/linux-$pkgver-mc$pkgrel$_kflavour" \
		"$subpkgdir"/lib/modules/$pkgver-mc$pkgrel-easy$_kflavour/source
}

src() {
	pkgdesc="Kernel source code used to build the kernel"
	provides="easy-kernel$_kflavour-src=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir"/usr/src
	mv "$srcdir"/linux-src "$subpkgdir"/usr/src/linux-$pkgver-mc$pkgrel$_kflavour
}

sha512sums="77e43a02d766c3d73b7e25c4aafb2e931d6b16e870510c22cef0cdb05c3acb7952b8908ebad12b10ef982c6efbe286364b1544586e715cf38390e483927904d8  linux-4.14.tar.xz
61eda169a4bd195202f1cf82a6d7c8ff10e420cfa1ad67ab8346ed868e154196e9c13002bf4849d6afe6d7115bcd139c6bb395791446ec2f69aa2adebfdf675f  linux-4.14-mc14.patch.xz
e41d9111219342ad13367902242444ecdd847a93575c3f9709d6c6a075bc650f4a15be9db1a8798435fc0a0b56d41705829bfe0d2c2d88f8d1c28931e27ef5be  ast-endianness.patch
422336fc20b3324a9de9009a4d57a7747d6e3140dac317ddfd22ee0c560dc58cf47eb7374fe75a636b8e05d58c97b0ceed52ab5c4607ce6354e8650c882d73cc  config-ppc64"
