# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=git
pkgver=2.22.0
pkgrel=0
pkgdesc="A distributed version control system"
url="https://www.git-scm.com/"
arch="all"
license="GPL-2.0+"
depends="perl-error"
replaces="git-perl perl-git"

# secfixes:
#   2.19.2:
#   - CVE-2018-19486
#   2.14.1:
#   - CVE-2017-1000117

# note that order matters
subpackages="$pkgname-doc
	$pkgname-bash-completion:completion:noarch
	$pkgname-email
	$pkgname-fast-import:_fast_import
	$pkgname-cvs::noarch
	$pkgname-p4::noarch
	$pkgname-daemon
	$pkgname-gitweb
	$pkgname-subtree::noarch
	$pkgname-subtree-doc:subtree_doc:noarch
	"
makedepends="zlib-dev openssl-dev curl-dev expat-dev perl-dev
	pcre2-dev asciidoctor xmlto perl-error"
checkdepends="python3"
source="https://www.kernel.org/pub/software/scm/git/git-$pkgver.tar.xz
	dont-test-other-encodings.patch
	git-daemon.initd
	git-daemon.confd
	"
_gitcoredir=/usr/libexec/git-core

prepare() {
	default_prepare
	cat >> config.mak <<-EOF
		NO_GETTEXT=YesPlease
		NO_SVN_TESTS=YesPlease
		NO_REGEX=YesPlease
		USE_ASCIIDOCTOR=1
		USE_LIBPCRE2=YesPlease
		NO_NSEC=YesPlease
		NO_SYS_POLL_H=1
		CFLAGS=$CFLAGS
	EOF
}

build() {
	make prefix=/usr DESTDIR="$pkgdir"
}

check() {
	rm t/t9020-remote-svn.sh  # Requires /usr/bin/python / python-compat.
	make prefix=/usr DESTDIR="$pkgdir" -j1 test
}

package() {
	make -j1 prefix=/usr \
		DESTDIR="$pkgdir" \
		INSTALLDIRS=vendor \
		install
	mkdir -p "$pkgdir"/var/git
	install -Dm755 "$srcdir"/git-daemon.initd \
		"$pkgdir"/etc/init.d/git-daemon
	install -Dm644 "$srcdir"/git-daemon.confd \
		"$pkgdir"/etc/conf.d/git-daemon

	make prefix=/usr DESTDIR="$pkgdir" install-man
	find "$pkgdir" -name perllocal.pod -delete
}

email() {
	depends="perl perl-net-smtp-ssl perl-authen-sasl"
	pkgdesc="Git tools for sending email"
	replaces="git"
	mkdir -p "$subpkgdir"/$_gitcoredir
	mv "$pkgdir"/$_gitcoredir/*email* "$pkgdir"/$_gitcoredir/*imap* \
		"$subpkgdir"/$_gitcoredir
}

cvs() {
	pkgdesc="Git tools for importing CVS repositories"
	depends="perl cvs perl-dbd-sqlite"
	replaces="git-perl"
	mkdir -p "$subpkgdir"/usr/bin "$subpkgdir"/$_gitcoredir
	mv "$pkgdir"/usr/bin/git-cvs* "$subpkgdir"/usr/bin/
	mv "$pkgdir"/$_gitcoredir/*cvs* "$subpkgdir"/$_gitcoredir \

}

_fast_import() {
	pkgdesc="Git backend for fast Git data importers"
	depends="git=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir"/$_gitcoredir
	mv "$pkgdir"/$_gitcoredir/git-fast-import "$subpkgdir"/$_gitcoredir/
}

p4() {
	pkgdesc="Git tools for working with Perforce depots"
	depends="git=$pkgver-r$pkgrel git-fast-import=$pkgver-r$pkgrel"
	replaces="git"
	mkdir -p "$subpkgdir"/$_gitcoredir/mergetools
	mv "$pkgdir"/$_gitcoredir/*p4* "$subpkgdir"/$_gitcoredir/
	mv "$pkgdir"/$_gitcoredir/mergetools/*p4* \
		"$subpkgdir"/$_gitcoredir/mergetools/
}

daemon() {
	pkgdesc="Git protocol daemon"
	depends="git=$pkgver-r$pkgrel"
	replaces="git"
	mkdir -p "$subpkgdir"/$_gitcoredir
	mv "$pkgdir"/$_gitcoredir/git-daemon \
		"$pkgdir"/$_gitcoredir/git-http-backend \
		"$pkgdir"/$_gitcoredir/git-shell \
		"$subpkgdir"/$_gitcoredir \

	mv "$pkgdir"/etc "$subpkgdir"/
}

gitweb() {
	pkgdesc="Simple web interface to git repositories"
	depends="git=$pkgver-r$pkgrel perl"
	replaces="git"
	mkdir -p "$subpkgdir"/usr/share "$subpkgdir"$_gitcoredir
	mv "$pkgdir"/usr/share/gitweb "$subpkgdir"/usr/share/
	mv "$pkgdir"/$_gitcoredir/git-instaweb "$subpkgdir"$_gitcoredir
}

completion() {
	pkgdesc="Bash completion for $pkgname"
	depends=""
	replaces=""
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	install -Dm644 "$builddir"/contrib/completion/git-completion.bash \
		"$subpkgdir"/usr/share/bash-completion/completions/git
}

subtree() {
	depends="git=$pkgver-r$pkgrel"
	pkgdesc="Split git repository into subtrees"
	replaces=""

	cd "$builddir"/contrib/subtree
	make prefix=/usr DESTDIR="$pkgdir"
	make install prefix=/usr DESTDIR="$subpkgdir"
}

subtree_doc() {
	depends=""
	pkgdesc="Split git repository into subtrees (documentation)"
	replaces=""

	cd "$builddir"/contrib/subtree
	make install-man prefix=/usr DESTDIR="$subpkgdir"
	gzip -9 "$subpkgdir"/usr/share/man/man1/git-subtree.1
}

sha512sums="75b3dcac36f80281effcd099944de34050a35f3599ce42f86ce60455b6c952039fb0f6438d296e0cc9c0651d4a17f467780dc475669227d3c98ddefe91723d42  git-2.22.0.tar.xz
315e4666c425b31a7f7a447e777cbf2a1050feac9b4d4b8a61c05248024e790d4d18f3336faf2a2c68584e05e8194c22a09e1caba1352cfec194e0bc01277a6c  dont-test-other-encodings.patch
89528cdd14c51fd568aa61cf6c5eae08ea0844e59f9af9292da5fc6c268261f4166017d002d494400945e248df6b844e2f9f9cd2d9345d516983f5a110e4c42a  git-daemon.initd
fbf1f425206a76e2a8f82342537ed939ff7e623d644c086ca2ced5f69b36734695f9f80ebda1728f75a94d6cd2fcb71bf845b64239368caab418e4d368c141ec  git-daemon.confd"
