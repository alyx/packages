# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Horst Burkhardt <horst@adelielinux.org>
pkgname=sudo
pkgver=1.8.27
if [ "${pkgver%_*}" != "$pkgver" ]; then
	_realver=${pkgver%_*}${pkgver#*_}
else
	_realver=$pkgver
fi
pkgrel=0
pkgdesc="Give certain users the ability to run some commands as root"
url="https://www.sudo.ws/sudo/"
arch="all"
options="suid"
license="ISC AND MIT AND BSD-3-Clause AND BSD-2-Clause AND Zlib"
depends=""
makedepends_host="linux-pam-dev zlib-dev utmps-dev"
makedepends_build="bash"
makedepends="$makedepends_host $makedepends_build"
subpackages="$pkgname-doc $pkgname-dev $pkgname-lang"
source="https://www.sudo.ws/dist/sudo-${_realver}.tar.gz
	fix-cross-compile.patch
	musl-fix-headers.patch
	test-suite.patch
	"
builddir="$srcdir"/$pkgname-$_realver

# secfixes:
#   1.8.20_p2-r0:
#   - CVE-2017-1000368

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libexecdir=/usr/lib \
		--mandir=/usr/share/man \
		--enable-pie \
		--with-env-editor \
		--with-pam \
		--without-skey \
		--with-passprompt="[sudo] Password for %p: "
	make
}

check() {
	make check
}

package() {
	# the sudo's mkinstalldir script miscreates the leading
	# path components with bad permissions. fix this.
	install -d -m0755 "$pkgdir"/var "$pkgdir"/var/db
	make -j1 DESTDIR="$pkgdir" install
	rm -rf "$pkgdir"/var/run
}

sha512sums="0480def650ab880ab9e6c51c606a06897fd638f0381e99c038f5aa47d064aaa2fb35b73eee7f86e73185e18d5dbb8b6ba49c616b1785a1edb2dd6d7b2fa4fcac  sudo-1.8.27.tar.gz
f0f462f40502da2194310fe4a72ec1a16ba40f95a821ba9aa6aabaa423d28c4ab26b684afa7fb81c2407cf60de9327bdab01de51b878c5d4de49b0d62645f53c  fix-cross-compile.patch
dcc03abdd672c934f90dfd3683b3f81a8d39cfff91307d2dbd20a31a852022ab605d034c4fe11860ba99b78d391a9812fca1d6e052620b8ff2c42e4f0c7a1a62  musl-fix-headers.patch
3feb9f586fb36cdddaf61523926554533d73b4b03e7bfe5da40b754d68c4cc58c796310cc3a3419dd0d87d30d836d5f9921f5ea3c7c7a0099626f49c161279dc  test-suite.patch"
