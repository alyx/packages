# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=s6
pkgver=2.8.0.1
pkgrel=1
pkgdesc="skarnet.org's small & secure supervision software suite"
url="https://skarnet.org/software/$pkgname/"
arch="all"
options="!check"  # No test suite.
license="ISC"
_skalibs_version=2.8.1.0
depends="execline"
makedepends="skalibs-dev>=$_skalibs_version execline-dev"
install="$pkgname.post-upgrade"
subpackages="$pkgname-libs $pkgname-dev $pkgname-libs-dev:libsdev $pkgname-doc"
triggers="$pkgname.trigger=/run/service"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz"

build() {
	cd "$builddir"
	./configure \
		--enable-shared \
		--enable-static \
		--enable-allstatic \
		--enable-static-libc \
		--libdir=/usr/lib \
		--libexecdir="/lib/$pkgname" \
		--with-dynlib=/lib
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	mkdir -p -m 0755 "$pkgdir/var/lib/s6/services"
}


libs() {
        pkgdesc="$pkgdesc (shared libraries)"
        depends="skalibs-libs>=$_skalibs_version"
        mkdir -p "$subpkgdir/lib"
        mv "$pkgdir"/lib/*.so.* "$subpkgdir/lib/"
}


dev() {
        pkgdesc="$pkgdesc (development files)"
        depends="skalibs-dev>=$_skalibs_version"
        mkdir -p "$subpkgdir/usr"
        mv "$pkgdir/usr/lib" "$pkgdir/usr/include" "$subpkgdir/usr/"
}


libsdev() {
        pkgdesc="$pkgdesc (development files for dynamic linking)"
        depends="$pkgname-dev"
        mkdir -p "$subpkgdir/lib"
        mv "$pkgdir"/lib/*.so "$subpkgdir/lib/"
}


doc() {
        pkgdesc="$pkgdesc (documentation)"
        depends=
        install_if="docs $pkgname=$pkgver-r$pkgrel"
        mkdir -p "$subpkgdir/usr/share/doc"
        cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}

sha512sums="03478aed129c1e50b34e0b75d7ff50bd84f62eadee862b6227c4313153d47776e7cbeae728d63209773b91931a2abc8372bb7db4953762807d0ed3d305efd23f  s6-2.8.0.1.tar.gz"
