# Maintainer: Adelie Platform Group <adelie-devel@lists.adelielinux.org>
pkgname=gmp
pkgver=6.1.2
pkgrel=1
pkgdesc="A free library for arbitrary precision arithmetic"
url="https://gmplib.org/"
arch="all"
license="LGPL-3.0+ OR GPL-2.0+"
makedepends="m4 texinfo libtool"
depends=
subpackages="$pkgname-doc $pkgname-dev libgmpxx"
source="https://gmplib.org/download/gmp/gmp-$pkgver.tar.xz
	"

prepare() {
	cd "$builddir"
	default_prepare
	# force update to libtool with fixed cross-build support
	libtoolize -f
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--with-sysroot=$CBUILDROOT \
		--prefix=/usr \
		--infodir=/usr/share/info \
		--mandir=/usr/share/man \
		--localstatedir=/var/state/gmp \
		--enable-cxx \
		--with-pic
	make
}

package() {
	cd "$builddir"
	make -j1 DESTDIR="${pkgdir}" install
}

check() {
	cd "$builddir"
	[ "$CBUILD" = "$CHOST" ] && make check
}

libgmpxx() {
	pkgdesc="C++ support for gmp"
	mkdir -p "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgmpxx.so.* "$subpkgdir"/usr/lib/
}

sha512sums="9f098281c0593b76ee174b722936952671fab1dae353ce3ed436a31fe2bc9d542eca752353f6645b7077c1f395ab4fdd355c58e08e2a801368f1375690eee2c6  gmp-6.1.2.tar.xz"
