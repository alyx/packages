#!/bin/sh -e

# This script performs a "poor man's reboot" when the
# init infrastructures are not there to do a proper one.
# This is the case when the user *just* switched between
# sysvinit and s6-linux-init, and needs to shut down the
# old system but the shutdown binaries are the ones from
# the new system.
#
# Always launch it from a console tty (tty1, ... tty6),
# never from a network terminal or an xterm.

trap "" INT HUP TERM
mkdir -p -m 0755 /etc/runlevels/empty2
/sbin/openrc empty2
rmdir /etc/runlevels/empty2
sync
/etc/init.d/killprocs start
/etc/init.d/savecache start
/etc/init.d/mount-ro start
/sbin/reboot -f
