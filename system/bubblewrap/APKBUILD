# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=bubblewrap
pkgver=0.3.3
pkgrel=0
pkgdesc="Unprivileged sandboxing tool"
url="https://github.com/projectatomic/bubblewrap"
arch="all"
options="!check suid"  # requires suid to already be set in order to check
license="LGPL-2.0+"
makedepends="autoconf automake libcap-dev docbook-xsl"
checkdepends="sudo"
subpackages="$pkgname-doc $pkgname-bash-completion:bashcomp:noarch"
source="bubblewrap-$pkgver.tar.gz::https://github.com/projectatomic/bubblewrap/archive/v$pkgver.tar.gz
	realpath-workaround.patch
	musl-fixes.patch
	tests.patch"

# secfixes:
#   0.3.3-r0:
#   - CVE-2019-12439

prepare() {
	cd "$builddir"
	srcdir= NOCONFIGURE=1 ./autogen.sh
	default_prepare
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--with-priv-mode=setuid
	make
}

check() {
	# Uses sudo to chown root and setuid $builddir/test-bwrap
	#
	# As of 0.3.3-r0, all tests pass on ppc64 except those relating
	# to bind mounts over symlinks. Those tests fail because musl's
	# realpath depends on the availability of /proc, which is not
	# available in the middle of the setup procedure since pivot_root
	# has been performed at least once. They have been patched to be
	# skipped.
	make check
}

package() {
	make install DESTDIR="$pkgdir"
}

bashcomp() {
	pkgdesc="Bash completions for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	mkdir -p "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/bash-completion/ "$subpkgdir"/usr/share/
}

sha512sums="b1c38fad90ddaa23a5f2dd49f9ec3f9d9af7426af321ae9f7c43dd64f11a448b3502942a42112a1c6ebf8a4dea2e1196b17c31cca9c2f119dc2e0c1674c345ae  bubblewrap-0.3.3.tar.gz
400a0446670ebf80f16739f1a7a2878aadc3099424f957ba09ec3df780506c23a11368f0578c9e352d7ca6473fa713df826fad7a20c50338aa5f9fa9ac6b84a4  realpath-workaround.patch
f59cda3b09dd99db9ca6d97099a15bb2523e054063d677502317ae3165ba2e32105a0ae8f877afc3827bd28d093c9d9d413270f4c87d9fe5f26f3eee670d916e  musl-fixes.patch
d572a6296729ab192dd4f04707e0271df600d565897ce089b7f00b9ae6c62e71a087e864b4c4972e0a64aeb222a337ff4ed95560620c200cc44534db1ca79efd  tests.patch"
