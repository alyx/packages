# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=sed
pkgver=4.7
pkgrel=0
pkgdesc="GNU stream editor"
url="https://www.gnu.org/software/sed"
arch="all"
license="GPL-3.0+"
makedepends="perl"
subpackages="$pkgname-doc $pkgname-lang"
install="$pkgname.post-deinstall"
source="https://ftp.gnu.org/pub/gnu/$pkgname/$pkgname-$pkgver.tar.xz
	disable-mbrtowc-test.patch
	gnulib-tests-dont-require-gpg-passphrase.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--bindir=/bin \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-i18n
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	rm -rf "$pkgdir"/usr/lib/charset.alias || true
	rmdir -p "$pkgdir"/usr/lib 2>/dev/null || true
}

sha512sums="e0be5db4cdf8226b34aaa9071bc5ae0eafde1c52227cee3512eea7fe2520d6c5cebf15266aa5c4adffbb51bf125c140a15644e28d57759893c12823ea9bbf4fb  sed-4.7.tar.xz
aeb55f85a5c724f0dacbf2f39e0f99ae4c66159115b00aa36d65f234f87e52e660878cb18b772a494349632dfa1b616b9306a4cafe87e91182ea8936c308506a  disable-mbrtowc-test.patch
8dc38bbd731885c69e83d2ed45d5df28542d9401f196295871135aa157d48ac77f1f926afcaab3ee0098ab0eb6232efc4664060177c3bf109a3e673f5adf9b85  gnulib-tests-dont-require-gpg-passphrase.patch"
