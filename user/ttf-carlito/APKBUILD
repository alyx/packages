# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=ttf-carlito
pkgver=20130920
pkgrel=0
pkgdesc="Google's metric-compatible replacement for Microsoft Calibri"
url="https://chromium.googlesource.com/chromiumos/overlays/chromiumos-overlay/+/master/media-fonts/crosextrafonts-carlito"
arch="noarch"
options="!check"  # You can't test fonts. -.-
license="OFL-1.1 AND MIT"
depends="fontconfig encodings mkfontdir mkfontscale"
makedepends="font-util-dev"
subpackages=""
# added fontconfig configuration (MIT) from:
# https://github.com/bohoomil/fontconfig-ultimate/tree/master/fontconfig_patches/fonts-settings
source="http://commondatastorage.googleapis.com/chromeos-localmirror/distfiles/crosextrafonts-${pkgname#ttf-}-$pkgver.tar.gz
	45-carlito.conf
	90-tt-carlito.conf"
builddir="$srcdir/crosextrafonts-${pkgname#ttf-}-$pkgver"

package() {
	mkdir -p "$pkgdir"/usr/share/fonts/$pkgname \
		"$pkgdir"/etc/fonts/conf.avail \
		"$pkgdir"/etc/fonts/conf.d

	install -m644 "$builddir"/*.ttf "$pkgdir"/usr/share/fonts/$pkgname

	cd "$pkgdir"/etc/fonts/conf.d
	for j in "$srcdir"/*.conf; do
		install -m644 "$j" "$pkgdir"/etc/fonts/conf.avail/
		ln -sf /etc/fonts/conf.avail/${j##*/}
	done
}

sha512sums="eb516060487d30353e06de711921c0d54c5844f2be69b1c1d37edac4b27303e9213a6bb306f309dc5827b2a6a2a37c9cb85b62368164eba6dd397dbaab3e3ba0  crosextrafonts-carlito-20130920.tar.gz
1eb50a4f3bf1aa6c229059ef25a1344824ac3be179fa819f6613d1457758b4d79ae63e1872a4b54a32bc413d84f7a362c1f9fd9c8cd1265bfcf7abfc3f1b9c27  45-carlito.conf
b859c5996a4942674979d4353e63ca59a383829e97713512a59910869841dbc923708880af74d9fdebb50ce482e21a2a8c8b1716973a676ee76f6248b3d1521d  90-tt-carlito.conf"
