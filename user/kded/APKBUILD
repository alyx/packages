# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kded
pkgver=5.54.0
pkgrel=0
pkgdesc="Central KDE workspace daemon"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1-only AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules kconfig-dev kcoreaddons-dev
	kcrash-dev kdbusaddons-dev kdoctools-dev kinit-dev kservice-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kded-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="696f98b222616200a308c97c04d3fcc1f941bd46bf4ff8913d5d3e95c46f385906dc71c43e55d44938aab21a572786130370b5a1210a811390c356a50a2ccc77  kded-5.54.0.tar.xz"
