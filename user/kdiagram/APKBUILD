# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdiagram
pkgver=2.6.1
pkgrel=0
pkgdesc="Charting libraries used by KDE"
url="https://www.kde.org/"
arch="all"
options="!check"  # Tests require X11
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="cmake extra-cmake-modules qt5-qtsvg-dev qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/kdiagram/$pkgver/kdiagram-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="caa1f6f8bffb4b311224edfed115f189fa9c88e4b9d8bc9d4b7bc963c6b8c896813b438ad89fa9e91e4cde2f55ae822cfc7ba559bbdad517c60a037591dc5128  kdiagram-2.6.1.tar.xz"
