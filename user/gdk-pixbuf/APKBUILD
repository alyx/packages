# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gdk-pixbuf
pkgver=2.38.1
pkgrel=0
pkgdesc="GTK+ image loading library"
url="https://www.gtk.org/"
arch="all"
options="!check"  # bug753605-atsize.jpg is missing from tarball.
license="LGPL-2.0+"
depends="shared-mime-info"
makedepends="glib-dev gobject-introspection-dev libjpeg-turbo-dev libpng-dev
	meson ninja python3 tiff-dev xmlto"
install="$pkgname.pre-deinstall"
triggers="$pkgname.trigger=/usr/lib/gdk-pixbuf-2.0/*/loaders"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.gnome.org/sources/gdk-pixbuf/${pkgver%.*}/gdk-pixbuf-$pkgver.tar.xz
	"
replaces="gtk+"

# secfixes:
#   2.36.6-r1:
#     - CVE-2017-6311
#     - CVE-2017-6312
#     - CVE-2017-6314

build() {
	mkdir build
	cd build
	meson -Dprefix=/usr -Dinstalled_tests=false
	ninja
}

check() {
	ninja -C build test
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
}

sha512sums="e50badaad2ccb3c816ba0849be386ecb75ea3c7df203d0b0144541c463ad1ace86c0d190b7550e3f59f0cba9639acc5cc048b7127484287894e4df6d62777920  gdk-pixbuf-2.38.1.tar.xz"
