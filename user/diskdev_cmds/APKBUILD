# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=diskdev_cmds
pkgver=332.14
pkgrel=0
pkgdesc="HFS+ utilities ported from OS X 10.8.4 (fsck/mkfs)"
url="https://opensource.apple.com/release/mac-os-x-10411ppc.html"
arch="all"
license="APSL-2.0"
depends=""
makedepends="bsd-compat-headers openssl-dev"
install=""
options="!check"
subpackages="$pkgname-doc"
source="https://opensource.apple.com/tarballs/diskdev_cmds/diskdev_cmds-$pkgver.tar.gz
	linux.patch
	musl.patch
	"
builddir="$srcdir/diskdev_cmds-$pkgver"

build() {
	cd "$builddir"
	make -f Makefile.lnx
}

package() {
	cd "$builddir"
	install -D -m755 "$builddir"/fsck_hfs.tproj/fsck_hfs "$pkgdir"/sbin/fsck_hfs
	install -m755 "$builddir"/newfs_hfs.tproj/newfs_hfs "$pkgdir"/sbin/newfs_hfs
	ln -s fsck_hfs "$pkgdir"/sbin/fsck.hfs
	ln -s fsck_hfs "$pkgdir"/sbin/fsck.hfsplus
	ln -s newfs_hfs "$pkgdir"/sbin/mkfs.hfs
	ln -s newfs_hfs "$pkgdir"/sbin/mkfs.hfsplus

	install -D -m644 "$builddir"/fsck_hfs.tproj/fsck_hfs.8 "$pkgdir"/usr/share/man/man8/fsck_hfs.8
	ln -s fsck_hfs.8 "$pkgdir"/usr/share/man/man8/fsck.hfs.8
	ln -s fsck_hfs.8 "$pkgdir"/usr/share/man/man8/fsck.hfsplus.8
	install -m644 "$builddir"/newfs_hfs.tproj/newfs_hfs.8 "$pkgdir"/usr/share/man/man8/newfs_hfs.8
	ln -s newfs_hfs.8 "$pkgdir"/usr/share/man/man8/mkfs.hfs.8
	ln -s newfs_hfs.8 "$pkgdir"/usr/share/man/man8/mkfs.hfsplus.8
}

sha512sums="e1df91fb5330dc01a79de99dd8b1f389a337a19c2136994d59b7cbca9c2e1cb5f7d47101366dd01db4e23a5d3d7afab41758045bf7402d739fa22182dc922044  diskdev_cmds-332.14.tar.gz
bd736c087291024d352d7e3379069f440cf1af6abbcb236499a00c6a756c1b2b3c36e61a13d08e8db7319654b9cb1c04574a4dbda79cedeceb32660708900da9  linux.patch
0b6280e20b0e8d2e453505866af2f1423a4f0172de2e5a6b14feb82df3c659270d9af0320a098e41d405e5cf9111852926e8a4eb5bea1604c262b126917f442f  musl.patch"
