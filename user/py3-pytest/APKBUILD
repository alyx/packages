# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=py3-pytest
_pkgname=pytest
pkgver=3.9.1
pkgrel=0
pkgdesc="A python test library"
url="https://pypi.python.org/pypi/pytest"
arch="noarch"
options="!checkroot !check"  # Cyclic dependency with itself. Passes on x86_64
# Certified net clean
license="MIT"
depends="python3 py3-atomicwrites py3-attrs py3-more-itertools
	py3-pluggy py3-py py3-six"
makedepends="python3-dev"
#checkdepends="py3-pytest py3-hypothesis"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/p/$_pkgname/$_pkgname-$pkgver.tar.gz
	scm.patch"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	cd "$builddir"
	python3 setup.py build
}

check() {
	cd "$builddir"
	# You would think you could get away with "python3 -m pytest" but no...
	PYTHONPATH="$builddir/build/lib:$PYTHONPATH" pytest
}

package() {
	cd "$builddir"
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="0a3a6842fb549c8223f0ddf56ca28eb200a21a4cadf382fbcd2ab930c58d57c55c77d5869e84bd79bf611d959cba6d363ace3b5fd33d5ce44259fd580b1a9369  py3-pytest-3.9.1.tar.gz
7fc2cd75f55caa661fb4bed578955511ee38cd28a25fcb405c29eb7227a382c33c0fc52a4eeb8950315793dfc0eb02235b140415deebd113f24df1a320b25959  scm.patch"
