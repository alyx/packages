# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=extra-cmake-modules
pkgver=5.54.0
pkgrel=0
pkgdesc="CMake modules needed for KDE development"
url="https://www.kde.org/"
arch="all"
license="BSD-3-Clause"
options="!dbg"
depends=""
makedepends="cmake qt5-qtbase-dev qt5-qttools-dev qt5-qtdeclarative-dev
	qt5-qtquickcontrols"
subpackages=""
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/extra-cmake-modules-$pkgver.tar.xz
	posix.patch"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE \
		ctest -E '(relative_or_absolute_|KDEFetchTranslations|ECMToolchainAndroidTest|ECMPoQmToolsTest)'
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="2c592ab659dc2c1167111d394b976f7d008b9c3e8e18aea06001f4852f93d6a0e61c2ea5f6add2680e11f76da9f5f8c437afd350c5d3224cf1e9adba2dd765e1  extra-cmake-modules-5.54.0.tar.xz
0af703a9d4696080bc52032d1d341e86fd142bef91f3c1afa9cf180e1cf2ff3bba2ea47fd36b8f52135f056a202c58a08e7c7525f671d6993839865ce6aef0f6  posix.patch"
