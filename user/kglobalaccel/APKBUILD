# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kglobalaccel
pkgver=5.54.0
pkgrel=0
pkgdesc="Framework for implementing global shortcuts/accelerators"
url="https://www.kde.org/"
arch="all"
options="!check"  # Only test requires X11.
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kconfig-dev kcoreaddons-dev kcrash-dev
	kdbusaddons-dev"
makedepends="$depends_dev cmake extra-cmake-modules xcb-util-keysyms-dev doxygen
	libxcb-dev libx11-dev libxext-dev libice-dev qt5-qttools-dev graphviz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kglobalaccel-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="aaab8ba98ea7229ccd31da07efd21d16e1bd44da9534e05fee73473becc3a7098857335be00c30aa247f15454cb83cc2a0b11ab1c1385c8b682aebf06e0d6fce  kglobalaccel-5.54.0.tar.xz"
