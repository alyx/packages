# Contributor: Marek Benc <dusxmt@gmx.com>
# Maintainer: Marek Benc <dusxmt@gmx.com>
pkgname=mksh
pkgver=56c
pkgrel=0
pkgdesc="MirBSD Korn Shell, a free Korn Shell interpreter"
url="http://mirbsd.de/mksh"
arch="all"
license="MirOS OR ISC"
depends=""
makedepends=""
checkdepends="ed perl"
subpackages="$pkgname-doc"
install="mksh.post-install mksh.post-upgrade mksh.pre-deinstall"
source="http://www.mirbsd.org/MirOS/dist/mir/${pkgname}/${pkgname}-R${pkgver}.tgz"
builddir=$srcdir/$pkgname

build() {
	cd "$builddir"

	# Build the main shell:
	/bin/sh ./Build.sh -r
	mv test.sh test_mksh.sh

	# Build the compatibility/legacy shell:
	CFLAGS="$CFLAGS -DMKSH_BINSHPOSIX -DMKSH_BINSHREDUCED" \
		/bin/sh ./Build.sh -r -L
	mv test.sh test_lksh.sh
}

package() {
	cd "$builddir"

	mkdir -p "$pkgdir"/bin
	install -m 755 mksh "$pkgdir"/bin
	install -m 755 lksh "$pkgdir"/bin

	mkdir -p "$pkgdir"/usr/share/man/man1/
	install -m 644 mksh.1 "$pkgdir"/usr/share/man/man1/
	install -m 644 lksh.1 "$pkgdir"/usr/share/man/man1/

	mkdir -p "$pkgdir"/usr/share/doc/mksh/examples/
	install -m 644 dot.mkshrc "$pkgdir"/usr/share/doc/mksh/examples/
}

check() {
	cd "$builddir"

	echo "Running the test suite for mksh:"
	./test_mksh.sh

	echo "Running the test suite for lksh:"
	./test_lksh.sh
}

sha512sums="7e4cd2d24c6bca2ebad7c6b02d158188e766bd4693d93f5bed198d69238aef078d42ce911431ad4e419e4af55083047d823bfad53973db5c6d8a685482770135  mksh-R56c.tgz"
