# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libice
pkgver=1.0.9
pkgrel=4
pkgdesc="X11 Inter-Client Exchange library"
url="https://www.X.Org/"
arch="all"
license="MIT"
depends=
makedepends="libbsd-dev util-macros xmlto xorgproto-dev xtrans"
checkdepends="check-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://www.X.Org/releases/individual/lib/libICE-$pkgver.tar.bz2
	CVE-2017-2626.patch"
builddir="$srcdir/libICE-$pkgver"

# secfixes:
#   1.0.9-r4:
#     - CVE-2017-2626

prepare() {
	default_prepare
	autoreconf -vif
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--enable-ipv6 \
		--enable-docs \
		--with-xmlto \
		--without-fop
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="daa8126ee5279c08f801274a2754132762dea2a40f4733c4b0bf8e8bdad61cba826939a2e067beb3524e256a98a2b83f23c8d4643f3e75a284ab02cc73da41b7  libICE-1.0.9.tar.bz2
83e53a4b48c429c7fad8f4feba1b9261e1ff26d995a729e7d38f1aac29cf5f69ffeb83a1733f3e624b09ae0ee97f09be8380ab0d59fb51436e1b537461a6943c  CVE-2017-2626.patch"
