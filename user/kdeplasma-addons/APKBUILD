# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdeplasma-addons
pkgver=5.12.7
pkgrel=0
pkgdesc="Extra applets and toys for KDE Plasma"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.1-only"
depends="qt5-qtquickcontrols qt5-qtquickcontrols2"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtx11extras-dev kactivities-dev kconfig-dev kconfigwidgets-dev
	kcmutils-dev kcoreaddons-dev kdoctools-dev ki18n-dev knewstuff-dev
	kross-dev krunner-dev kservice-dev kunitconversion-dev
	kdelibs4support-dev plasma-framework-dev plasma-workspace-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/kdeplasma-addons-$pkgver.tar.xz"

prepare() {
	cd "$builddir"
	default_prepare
	mkdir -p build
}

build() {
	cd "$builddir"/build
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="7af90cee5605dd1b3548c033216b57128e3ef99043168a165b75d3a97fd54794182ffb97846bac91cf806ba4e1a82365a5d61c58f8537de43364c626de392bc7  kdeplasma-addons-5.12.7.tar.xz"
