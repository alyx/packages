# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libksysguard
pkgver=5.12.7
pkgrel=0
pkgdesc="KDE system monitor library"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test requires accelerated X11 session
license="LGPL-2.1+ AND (GPL-2.0-only OR GPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev kconfig-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 libx11-dev zlib-dev
	qt5-qtscript-dev ki18n-dev kauth-dev kcompletion-dev kconfigwidgets-dev
	kcoreaddons-dev kiconthemes-dev plasma-framework-dev kservice-dev
	kwindowsystem-dev kwidgetsaddons-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/libksysguard-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="78a063606567632c19a7f1c9d2e7d69634432f3e49bb08a3162805aeb94e0930f2d43e9372207512558b785b8c7e20fdca801de35c055e456ac3cb812bd96efa  libksysguard-5.12.7.tar.xz"
