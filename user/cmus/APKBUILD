# Contributor: Luis Ressel <aranea@aixah.de>
# Maintainer: Luis Ressel <aranea@aixah.de>
pkgname=cmus
pkgver=2.8.0
pkgrel=0
pkgdesc="An ncurses based music player with plugin support for many formats"
url="https://cmus.github.io/"
arch="all"
options="!check" # no test suite
license="GPL-2.0+"
depends=""
makedepends="alsa-lib-dev faad2-dev ffmpeg-dev flac-dev libao-dev libcddb-dev
	libcdio-paranoia-dev libmad-dev libmodplug-dev libvorbis-dev ncurses-dev
	opusfile-dev pulseaudio-dev wavpack-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/$pkgname/$pkgname/archive/v$pkgver.tar.gz
	ppc-libatomic.patch"

build() {
	cd "$builddir"
	./configure prefix=/usr \
		CONFIG_AAC=y \
		CONFIG_ALSA=y \
		CONFIG_AO=y \
		CONFIG_ARTS=n \
		CONFIG_BASS=n \
		CONFIG_CDDB=y \
		CONFIG_CDIO=y \
		CONFIG_COREAUDIO=n \
		CONFIG_CUE=y \
		CONFIG_DISCID=n \
		CONFIG_FFMPEG=y \
		CONFIG_FLAC=y \
		CONFIG_JACK=n \
		CONFIG_MAD=y \
		CONFIG_MIKMOD=n \
		CONFIG_MODPLUG=y \
		CONFIG_MP4=n \
		CONFIG_MPC=n \
		CONFIG_MPRIS=n \
		CONFIG_OPUS=y \
		CONFIG_OSS=y \
		CONFIG_PULSE=y \
		CONFIG_ROAR=n \
		CONFIG_SAMPLERATE=n \
		CONFIG_SNDIO=n \
		CONFIG_SUN=n \
		CONFIG_TREMOR=n \
		CONFIG_VORBIS=y \
		CONFIG_VTX=n \
		CONFIG_WAV=y \
		CONFIG_WAVEOUT=n \
		CONFIG_WAVPACK=y \
		USE_FALLBACK_IP=n

	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" PREFIX=/usr install
}

sha512sums="cf359dfcefa833a5b10a2d16ac405672bea762b62b7177c115560127035682fba65c15b9a8710179a343d1f99212a0260b5c095542982202e2cd1bef5b0c17fc  cmus-2.8.0.tar.gz
06a91da06cc916aa475467aa90bd7170c7288b68459706c3416700e79ba8707fd7a85bded0c0d5d51d805c15f59ff395670f11318ca5a419d17ab1070a48775f  ppc-libatomic.patch"
