# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kgpg
pkgver=19.04.2
pkgrel=0
pkgdesc="Simple interface for GnuPG, a powerful encryption utility"
url="https://utils.kde.org/projects/kgpg/"
arch="all"
options="!check"  # Tests require X11 now.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kcodecs-dev
	kdoctools-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev ki18n-dev
	kiconthemes-dev kjobwidgets-dev kio-dev knotifications-dev kservice-dev
	ktextwidgets-dev kxmlgui-dev kwidgetsaddons-dev kwindowsystem-dev
	akonadi-contacts-dev kcontacts-dev gpgme-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kgpg-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="3c9cce222727758a04cdee9be09ad4e679aa9dc199a08d2cd867f75e6bb9dabd1137c60e2720e27d3affca73e8bc872aca088568a066d3f988d72843ee1d9e94  kgpg-19.04.2.tar.xz"
