# Contributor: Brandon Bergren <git@bdragon.rtk0.net>
# Maintainer: Brandon Bergren <git@bdragon.rtk0.net>
pkgname=py3-freezegun
_pkgname=freezegun
pkgver=0.3.11
pkgrel=0
pkgdesc="Let your Python tests travel through time"
url="https://pypi.python.org/pypi/freezegun"
arch="noarch"
options="!check" # Disabled until 0.3.12 due to dependency on obsolete nose library. See https://github.com/spulec/freezegun/issues/280
license="Apache-2.0"
depends="python3 py3-six"
makedepends="python3-dev"
checkdepends="py3-python-dateutil py3-pytest py3-mock py3-pbr"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	cd "$builddir"
	python3 setup.py build
}

check() {
	cd "$builddir"
	python3 setup.py test
}

package() {
	cd "$builddir"
	python3 setup.py install --prefix=/usr --root="$pkgdir"

}

sha512sums="e8b392176641d52f6ed795c9af5fbc0a62892aeedf32b42375b56ab44a9ad7a5ecd3bb81363ed0ae65204aff2ef894cd7f2e17f42be72f31d3409b2bffa59ab8  py3-freezegun-0.3.11.tar.gz"
