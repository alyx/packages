# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kde-cli-tools
pkgver=5.12.7
pkgrel=0
pkgdesc="KDE command-like utilities"
url="https://www.kde.org/"
arch="all"
options="!check"  # MIME types for some reason think .doc == .txt
license="(GPL-2.0-only OR GPL-3.0-only) AND GPL-2.0+ AND GPL-2.0-only AND LGPL-2.1-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev
	qt5-qtx11extras-dev kactivities-dev kcmutils-dev kconfig-dev
	kdelibs4support-dev kdesu-dev kdoctools-dev ki18n-dev kiconthemes-dev
	kinit-dev kio-dev kwindowsystem-dev"
subpackages="$pkgname-lang $pkgname-doc"
source="https://download.kde.org/stable/plasma/$pkgver/kde-cli-tools-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="44dd8a9b999dd0f4fb9f5953da7b9af805ba0508b18bbcb8420418049318b095d9553b1723e1ddab8b7d3d48ed42ca75fb0ede17043c2a82c95769938130ad77  kde-cli-tools-5.12.7.tar.xz"
