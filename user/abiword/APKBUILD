# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: multiplexd <multi@in-addr.xyz>
pkgname=abiword
pkgver=3.0.2
pkgrel=0
pkgdesc="A fully-featured word processor"
url="https://www.abisource.com"
arch="all"
options="!check"  # Test suite requires valgrind, etc
license="GPL-2.0+"
makedepends="bzip2-dev enchant-dev fribidi-dev goffice-dev gtk+3.0-dev
	libgsf-dev libjpeg-turbo-dev librsvg-dev libxslt-dev pcre-dev
	popt-dev wv-dev"

# openxml plugin
makedepends="$makedepends boost-dev"

# collab plugin
makedepends="$makedepends gnutls-dev libsoup-dev dbus-glib-dev"

subpackages="$pkgname-dev $pkgname-doc"

_plugins="applix babelfish bmp clarisworks collab docbook eml epub \
	freetranslation garble gdict gimp google hancom hrtext iscii kword \
	latex loadbindings mht mif mswrite openwriter openxml opml paint \
	passepartout pdb pdf presentation s5 sdw t602 urldict wikipedia wml \
	xslfo"

source="https://www.abisource.com/downloads/$pkgname/$pkgver/source/$pkgname-$pkgver.tar.gz
	fix-black-drawing-regression.patch"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-shared \
		--disable-static \
		--enable-plugins="$_plugins"
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="29ce9e80b3b85ab2933e7d39216771d8c4e05db5255eaed0cf8e1d032ffaac2cb1880bf24e754196ad5dae4969a1c2101ce4dc9c1db14604adc2f852b6a17fe3  abiword-3.0.2.tar.gz
cae9a08047cd97d11acea25a2f0b0ca4e8e4556b462eb476507fa178a7412221839febfeb36bebf491bb94d53525c4584d86230f96c403a7ceb2dec4223be8fe  fix-black-drawing-regression.patch"
