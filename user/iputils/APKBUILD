# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=iputils
pkgver=20190515
pkgrel=0
pkgdesc="IP Configuration Utilities (and Ping)"
url="https://github.com/iputils/iputils/"
arch="all"
license="BSD-3-Clause AND GPL-2.0-or-later"
makedepends="meson libcap-dev libxslt docbook-xsl-ns openssl-dev libgcrypt-dev"
replaces="bbsuid"
subpackages="$pkgname-doc $pkgname-lang $pkgname-openrc"
source="$pkgname-$pkgver.tgz::https://github.com/$pkgname/$pkgname/archive/s$pkgver.tar.gz
	docbook-man.patch
	"
options="suid"
builddir="$srcdir/$pkgname-s$pkgver"

build() {
	cd "$builddir"
        # we're waiting for idn support in musl so that we can enable it here
        # https://wiki.musl-libc.org/functional-differences-from-glibc.html#Name-Resolver/DNS
	meson --prefix /usr --buildtype=plain builddir -DUSE_IDN=false
	ninja -v -C builddir
}

check() {
	cd "$builddir"
	ninja -C builddir test
}

package() {
	cd "$builddir"

	DESTDIR=$pkgdir ninja -C builddir install

	ln -s tracepath "$pkgdir"/usr/bin/tracepath6
	ln -s ping "$pkgdir"/usr/bin/ping6
}

sha512sums="adb8831ca3a567b9a5f3762227c631aefa62eedbaa7578c2bfea90b6d494b9e0cccf49b68713912611ec56c352d6c517df9e8409c9c9478cfc5732371c8cf250  iputils-20190515.tgz
411f45a2d16c88b09f3547e3f04aa20e257f54650c7783123e4cb021bd8850cd96896480fbf9305d6df72abe32d8d5c2239ce4ac4ef9bfa821c205309451d6ff  docbook-man.patch"
