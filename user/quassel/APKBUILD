# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=quassel
pkgver=0.13.1
pkgrel=1
pkgdesc="Modern, cross-platform IRC client"
url="https://quassel-irc.org/"
arch="all"
license="(GPL-2.0-only OR GPL-3.0-only) AND GPL-2.0+ AND LGPL-2.0+"
depends="$pkgname-client $pkgname-core"
makedepends="qt5-qtbase-dev qt5-qtmultimedia-dev libdbusmenu-qt-dev sonnet-dev
	extra-cmake-modules kconfigwidgets-dev kcoreaddons-dev kxmlgui-dev
	knotifications-dev knotifyconfig-dev ktextwidgets-dev qt5-qtscript-dev
	kwidgetsaddons-dev qca-dev qt5-qttools-dev zlib-dev libexecinfo-dev"
langdir="/usr/share/quassel/translations"
subpackages="$pkgname-core $pkgname-client $pkgname-lang"
source="https://quassel-irc.org/pub/quassel-$pkgver.tar.bz2
	unterminated-mIRC-codes.patch
	"

# secfixes:
#   0.12.5-r0:
#     - CVE-2018-1000178
#     - CVE-2018-1000179

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DUSE_QT5=ON \
		-DWITH_KDE=ON \
		-DWITH_WEBENGINE=OFF \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

client() {
	pkgdesc="Modern, cross-platform IRC client (Thin client only)"
	depends=""
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/quasselclient "$subpkgdir"/usr/bin/

	local dir
	for dir in {applications,icons,pixmaps,knotifications5}; do
		mkdir -p "$subpkgdir"/usr/share/$dir
		mv "$pkgdir"/usr/share/$dir "$subpkgdir"/usr/share/
	done

	mkdir -p "$subpkgdir"/usr/share/quassel
	mv "$pkgdir"/usr/share/quassel/stylesheets \
		"$subpkgdir"/usr/share/quassel/
}

core() {
	pkgdesc="Modern, cross-platform IRC daemon (core only)"
	depends=""
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/quasselcore "$subpkgdir"/usr/bin/
}

sha512sums="4ed55e81b1638c8851ddae5d9c9d23a1c2cea92f307e19f426873c2600d8e183898d3ed7c290f2ea5d1b8c5e1be7f9ffcc3e8c3c0193d080fc879b10cc3a962c  quassel-0.13.1.tar.bz2
7f5ffa64d0620061ac2b6eab2e163d5862d43b16e3d7572415b5819f9ee19a5482791c3cee20c3b99722f05ffed3f1f3ba6c82e7f78b6bea9170037675dd3b4a  unterminated-mIRC-codes.patch"
