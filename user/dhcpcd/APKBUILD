# Contributor: Michael Mason <ms13sp@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=dhcpcd
pkgver=7.2.2
pkgrel=0
pkgdesc="RFC2131 compliant DHCP client"
url="https://roy.marples.name/projects/dhcpcd"
arch="all"
license="BSD-2-Clause"
makedepends="linux-headers bsd-compat-headers eudev-dev"
install="$pkgname.post-upgrade"
subpackages="$pkgname-doc $pkgname-openrc"
source="https://roy.marples.name/downloads/dhcpcd/$pkgname-$pkgver.tar.xz
	fix-chrony-conf-location.patch
	dhcpcd.initd
	"

build() {
	cd "$builddir"

	CFLAGS="$CFLAGS -D_GNU_SOURCE -DHAVE_PRINTF_M"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--libexecdir=/usr/lib/$pkgname \
		--dbdir=/var/lib/$pkgname \
		--rundir=/run \
		--enable-ipv6
	make
}

check() {
	cd "$builddir"
	make test
}

package() {
	cd "$builddir"

	make DESTDIR="$pkgdir" install
	install -Dm755 "$srcdir"/dhcpcd.initd \
		"$pkgdir"/etc/init.d/dhcpcd
}

sha512sums="a774c4788efbb9712be04d3f29943b801043f9ec1ea1925282330afa00b9f3db5c29a85969ef004bf85b20045b4cc6ab241ceaae050a18051079d396845845e8  dhcpcd-7.2.2.tar.xz
1c19eed0f7a008ee96ea392beb327169ff8c83fc27fed20f65f05c9125f60629ebe3474c5e6a7cf4aeeea448fde4264c9b84916efacd67d47ab908c47b1fc3a5  fix-chrony-conf-location.patch
e777432c2efc84285b41e63a4687f3bd543f6864218d037529ab78b5ad934de154f28f478bd9facb56628f2953aad8a932bc2eb8b1dfffa0ce2278ffcfc4d880  dhcpcd.initd"
