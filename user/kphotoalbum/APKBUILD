# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kphotoalbum
pkgver=5.5
pkgrel=1
pkgdesc="Versatile photo album software by KDE"
url="https://www.kphotoalbum.org/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev phonon-dev karchive-dev
	kcompletion-dev kconfig-dev kcoreaddons-dev kdoctools-dev ki18n-dev
	kiconthemes-dev kjobwidgets-dev exiv2-dev ktextwidgets-dev kxmlgui-dev
	kwidgetsaddons-dev libjpeg-turbo-dev kio-dev libkipi-dev libkdcraw-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/kphotoalbum/$pkgver/kphotoalbum-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="fcca891a94580db2f606c5d077af2651e1dbcd59429823410f9fd2d07ac0c3c625ac441b723d4c8cb3ac7ede26ccf69f955c19fe302fffc9788fc33def9ee3e0  kphotoalbum-5.5.tar.xz"
