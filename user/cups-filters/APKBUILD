# Maintainer: Max Rees <maxcrees@me.com>
pkgname=cups-filters
pkgver=1.22.5
pkgrel=1
pkgdesc="OpenPrinting CUPS filters and backends"
url="https://wiki.linuxfoundation.org/openprinting/cups-filters"
arch="all"
license="GPL-2.0-only AND GPL-2.0+ AND GPL-3.0-only AND MIT"
depends="gnu-ghostscript poppler-utils bc ttf-freefont"
makedepends="cups-dev libjpeg-turbo-dev poppler-dev zlib-dev libpng-dev
	tiff-dev lcms2-dev freetype-dev fontconfig-dev qpdf-dev dbus-dev linux-headers
	coreutils gnutls-dev python3"
checkdepends="ttf-dejavu"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs"
source="https://www.openprinting.org/download/cups-filters/cups-filters-$pkgver.tar.xz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-static \
		--with-pdftops=pdftops \
		--with-shell=/bin/sh \
		--without-rcdir \
		--without-rclevels \
		--disable-avahi \
		--disable-mutool \
		--with-test-font-path='/usr/share/fonts/ttf-dejavu/DejaVuSans.ttf'
	# workaround parallel build issue by building libcupsfilters.la first
	make libcupsfilters.la && make libfontembed.la && make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
	# the pdf.utf-8 symlink isn't quite good enough
	cd "$pkgdir"/usr/share/cups/charsets && \
	ln -s pdf.utf-8.simple pdf.UTF-8
}

dev() {
	default_dev
	# cupsfilters.drv needs pcl.h
	install -Dm644 "$builddir"/filter/pcl.h \
		"$pkgdir"/usr/share/cups/ppdc/pcl.h
}

libs() {
	pkgdesc="OpenPrinting CUPS filters and backends - cupsfilters and fontembed libraries"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/lib*.so.* "$subpkgdir"/usr/lib/
}

sha512sums="ed33e6d6f090c00f0f0d1d012d3b394bfaa1f81feeb5b0b7ead1dffa2399f0165ce916a99c3bf28aca7f279bffba5b6575feff700130cabfcd46eb4c904d5c59  cups-filters-1.22.5.tar.xz"
