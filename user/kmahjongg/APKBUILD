# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmahjongg
pkgver=19.04.2
pkgrel=0
pkgdesc="Mah Jongg desktop game"
url="https://games.kde.org/game.php?game=kmahjongg"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	kcoreaddons-dev kcrash-dev kdbusaddons-dev kdeclarative-dev kxmlgui-dev
	kdoctools-dev knewstuff-dev libkdegames-dev libkmahjongg-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kmahjongg-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="159b269a1f4dfeda9b36f6f6b6f1d2a1510676c221061a123be643343c2407829eea342f6f784e80aa94190d46b111d7fd591945314164e5b7564681c8432225  kmahjongg-19.04.2.tar.xz"
