# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=juk
pkgver=19.04.2
pkgrel=0
pkgdesc="KDE Jukebox"
url="https://juk.kde.org/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	kcoreaddons-dev kcompletion-dev kcrash-dev kglobalaccel-dev ki18n-dev
	kiconthemes-dev kdoctools-dev kio-dev kjobwidgets-dev ktextwidgets-dev
	knotifications-dev kxmlgui-dev kwallet-dev kwidgetsaddons-dev
	kwindowsystem-dev taglib-dev phonon-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/juk-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="3d5b693105dbbe32788995a05cf013ebe5d7243d99e5f8c75f3148f9fc1a6604c660473db07cf2a289ab120a495ffd3e19fbb4c741633f2eafed57b04e0b2b1b  juk-19.04.2.tar.xz"
