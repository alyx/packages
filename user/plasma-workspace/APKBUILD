# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=plasma-workspace
pkgver=5.12.7
pkgrel=0
pkgdesc="KDE Plasma 5 workspace"
url="https://www.kde.org/plasma-desktop"
arch="all"
options="!check"  # Test requires X11 accelration.
license="(GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1+ AND GPL-2.0+ AND MIT AND LGPL-2.1-only AND LGPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.0-only"
# startkde shell script calls
depends="kinit qdbus qtpaths xmessage xprop xset xsetroot"
# font installation stuff
depends="$depends mkfontdir"
# QML deps
depends="$depends qt5-qtgraphicaleffects qt5-qtquickcontrols solid"
# other runtime deps / plugins
depends="$depends libdbusmenu-qt kcmutils kde-cli-tools kded kdesu kio-extras
	ksysguard kwin milou plasma-integration pulseaudio-utils iso-codes xrdb"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev kdelibs4support-dev
	kitemmodels-dev kservice-dev kwindowsystem-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qtscript-dev
	iso-codes-dev libdbusmenu-qt-dev libxtst-dev xcb-util-image-dev

	baloo-dev kactivities-dev kcmutils-dev kcrash-dev kdbusaddons-dev
	kdeclarative-dev kdesu-dev kdoctools-dev kglobalaccel-dev kholidays-dev
	kidletime-dev kjsembed-dev knewstuff-dev knotifyconfig-dev kpackage-dev
	krunner-dev kscreenlocker-dev ktexteditor-dev ktextwidgets-dev
	kwallet-dev kwayland-dev kwin-dev kxmlrpcclient-dev libksysguard-dev
	plasma-framework-dev prison-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-workspace-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING=OFF \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="e9d4a85b11264eebbce0845eefa5d01818e944c2179d5cbc3e23b720118894d88fbfe96467c68849a3fa89728cecff248cd7f9dfc0ab133e50dad28ecc957623  plasma-workspace-5.12.7.tar.xz"
