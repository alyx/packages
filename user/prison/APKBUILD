# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=prison
pkgver=5.54.0
pkgrel=0
pkgdesc="Qt Barcode library for programatically creating QR codes"
url="https://www.kde.org/"
arch="all"
license="MIT"
depends=""
depends_dev="qt5-qtbase-dev libqrencode-dev libdmtx-dev"
makedepends="$depends_dev cmake extra-cmake-modules doxygen graphviz
	qt5-qtdeclarative-dev qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-quick"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/prison-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

quick() {
	pkgdesc="$pkgdesc (QML binding)"
	mkdir -p "$subpkgdir"/usr/lib/qt5/
	mv "$pkgdir"/usr/lib/qt5/qml "$subpkgdir"/usr/lib/qt5/
}

sha512sums="6ffdeb4c8196a9ec6c845ebe1d58801c02d570f831bf5c1a2bd8f30d048cc0d62aa8e8273fc8db4689c6ac45caa35906bad82b211e58108b7ed8cf42649acf16  prison-5.54.0.tar.xz"
