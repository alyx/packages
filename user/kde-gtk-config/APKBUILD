# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kde-gtk-config
pkgver=5.12.7
pkgrel=0
pkgdesc="KDE System Settings panel for configuring GTK+ application styles"
url="https://www.kde.org/"
arch="all"
license="GPL-3.0+"
depends="gsettings-desktop-schemas"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev gtk+2.0-dev gtk+3.0-dev
	karchive-dev kcmutils-dev kconfigwidgets-dev ki18n-dev kiconthemes-dev
	kio-dev knewstuff-dev gsettings-desktop-schemas-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/kde-gtk-config-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="b6698fba63f5a9d3d58112ecd2c198e9edb9b0ad9d6c8a670bef3f0e836fcf0c5f2c5444d6cbdf7370f2bfcfe5ad8a770b513ebb7a693c19624bf52bf9d02056  kde-gtk-config-5.12.7.tar.xz"
