# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=libvisio
pkgver=0.1.6
pkgrel=3
pkgdesc="Import filter and tools for MS Visio diagrams"
url="https://wiki.documentfoundation.org/DLP/Libraries/libvisio"
arch="all"
license="MPL-2.0"
depends_dev="librevenge-dev"
makedepends="$depends_dev boost-dev icu-dev libxml2-dev doxygen gperf perl"
checkdepends="cppunit-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools"
source="http://dev-www.libreoffice.org/src/$pkgname/$pkgname-$pkgver.tar.xz"

prepare() {
	default_prepare
	update_config_sub
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

tools() {
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr
}

sha512sums="58cee8cfe4205b2cad2f11dbe17882e57ebf0d10500116ca9d8d120e138f8eb0c65a5fea3bd7d2746bf8140377ee9deb34258597e028f9fdc8d21f270606cce1  libvisio-0.1.6.tar.xz"
