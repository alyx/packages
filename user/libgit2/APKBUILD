# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Pierre-Gilas MILLON <pgmillon@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libgit2
pkgver=0.27.7
pkgrel=0
pkgdesc="Pure C re-entrant library for custom Git applications"
url="https://libgit2.org/"
arch="all"
license="GPL-2.0-only"
depends=""
depends_dev="curl-dev libssh2-dev"
makedepends="$depends_dev python3 cmake zlib-dev openssl-dev"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/$pkgname/$pkgname/archive/v$pkgver.tar.gz
	test-32bit.patch
	"

# secfixes:
#   0.27.3-r0:
#   - CVE-2018-10887
#   - CVE-2018-10888
#   - CVE-2018-11235
#   0.25.1-r0:
#   - CVE-2016-10128
#   - CVE-2016-10129
#   - CVE-2016-10130
#   0.24.3-r0:
#   - CVE-2016-8568
#   - CVE-2016-8569

build() {
	cd "$builddir"
	cmake \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS"
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="de2e266939bd40bc580603539e1156906b97299523336ddc6a66c3bec26729495bef2daa2d240b83b7e011e93852381e95a4407132b0440a5aa1e1b7642c0011  libgit2-0.27.7.tar.gz
83b82b442d34b633d1889863472e51f3b6b68fbd4089d3448d86adeb7ce79eee495fea3c19edb88f563226bd2badb0166b7e41f9d4ffe8304bbf3b2abdf5a22f  test-32bit.patch"
