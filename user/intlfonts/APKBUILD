# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=intlfonts
pkgver=1.2.1
pkgrel=0
pkgdesc="GNU typefaces for a wide variety of scripts"
url="https://directory.fsf.org/wiki/Intlfonts"
arch="noarch"
options="!check"  # No test suite.
license="GPL-2.0-only"
depends="fontconfig mkfontdir mkfontscale"
makedepends="bdftopcf"
subpackages=""
source="https://ftp.gnu.org/gnu/intlfonts/intlfonts-$pkgver.tar.gz"

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-compress \
		--with-bdf=no \
		--with-fontdir="$pkgdir"/usr/share/fonts/X11/misc \
		--with-truetype=yes
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	mv "$pkgdir"/usr/share/fonts/X11/misc/TrueType \
		"$pkgdir"/usr/share/fonts/X11/TTF
	rm "$pkgdir"/usr/share/fonts/X11/misc/fonts.{alias,dir}

	# font-daewoo-misc
	rm "$pkgdir"/usr/share/fonts/X11/misc/hanglg16.pcf.gz
	rm "$pkgdir"/usr/share/fonts/X11/misc/hanglm16.pcf.gz
	rm "$pkgdir"/usr/share/fonts/X11/misc/hanglm24.pcf.gz

	# font-isas-misc
	rm "$pkgdir"/usr/share/fonts/X11/misc/gb16fs.pcf.gz
	rm "$pkgdir"/usr/share/fonts/X11/misc/gb16st.pcf.gz
	rm "$pkgdir"/usr/share/fonts/X11/misc/gb24st.pcf.gz

	# font-jis-misc
	rm "$pkgdir"/usr/share/fonts/X11/misc/jiskan16.pcf.gz
	rm "$pkgdir"/usr/share/fonts/X11/misc/jiskan24.pcf.gz

	# font-misc-misc
	rm "$pkgdir"/usr/share/fonts/X11/misc/k14.pcf.gz

	# font-sony-misc
	rm "$pkgdir"/usr/share/fonts/X11/misc/8x16rk.pcf.gz
	rm "$pkgdir"/usr/share/fonts/X11/misc/12x24rk.pcf.gz
}

sha512sums="c11508335cf0f819e7907a6b16c305fbfbf1182dbdea939773482bda3c0f27d70ba427ca3d22c62f4190c24f35bb2bcde4e4fa185aff7f4964293263f7b4304e  intlfonts-1.2.1.tar.gz"
