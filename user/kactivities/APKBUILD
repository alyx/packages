# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kactivities
pkgver=5.54.0
pkgrel=0
pkgdesc="Runtime and library to organize work into separate activities"
url="https://api.kde.org/frameworks/kactivities/html/index.html"
arch="all"
license="GPL-2.0+ AND LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="kactivitymanagerd"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules doxygen graphviz boost-dev
	qt5-qttools-dev qt5-qtdeclarative-dev kconfig-dev kcoreaddons-dev
	kwindowsystem-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kactivities-$pkgver.tar.xz"

prepare() {
	cd "$builddir"
	default_prepare
	mkdir -p build
}

build() {
	cd "$builddir"/build
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"/build
	make test
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="d397c087f740e044a14f02bca47da2c267b4c9d9ab8231e240addf41eec6d716fc6d2a85bc3760636a69be55a058ba8d380c4555aa16ba43fff7fd2dfcb20a64  kactivities-5.54.0.tar.xz"
