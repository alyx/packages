# Contributor: William Pitcock <nenolod@dereferenced.org>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=raptor2
pkgver=2.0.15
pkgrel=1
pkgdesc="RDF parser/serializer toolkit for Redland"
url="http://www.librdf.org/raptor"
arch="all"
license="Public-Domain AND (LGPL-2.1+ OR GPL-2.0+ OR Apache-2.0+)"
depends=""
depends_dev="curl-dev libxml2-dev libxslt-dev yajl-dev"
makedepends="$depends_dev"
subpackages="$pkgname-dev $pkgname-doc"
source="http://download.librdf.org/source/$pkgname-$pkgver.tar.gz"

prepare() {
	default_prepare
	update_config_sub
	autoreconf -vif
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--target=$CTARGET \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var
	make
}

check() {
	cd "$builddir"
	# Note: some tests are shown as FAIL but they are actually XFAIL :)
	# As long as the return status is zero we are fine
	make check -j1
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="563dd01869eb4df8524ec12e2c0a541653874dcd834bd1eb265bc2943bb616968f624121d4688579cdce11b4f00a8ab53b7099f1a0850e256bb0a2c16ba048ee  raptor2-2.0.15.tar.gz"
