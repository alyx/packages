# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kmines
pkgver=19.04.2
pkgrel=0
pkgdesc="Classic Minesweeper game"
url="https://games.kde.org/game.php?game=kmines"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kconfigwidgets-dev
	kconfig-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev kdoctools-dev
	ki18n-dev ktextwidgets-dev kwidgetsaddons-dev kxmlgui-dev
	libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/applications/$pkgver/src/kmines-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="dd43246fc6ee85da08a4a7df7c53d84bb2b49c5e3d258f44798a7c1282e4d6b4ad693b659def1c9dd83d210ab7a254bc1051d3c55608d20d19e6321636700538  kmines-19.04.2.tar.xz"
