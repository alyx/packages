# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ppp
pkgver=2.4.7
pkgrel=0
pkgdesc="Point-to-Point Protocol (PPP) implementation for serial networking"
url="https://ppp.samba.org/"
arch="all"
options="!check"  # No test suite.
license="BSD-4-Clause AND GPL-2.0-only AND GPL-2.0+ AND zlib AND LGPL-2.0+"
depends=""
makedepends="bsd-compat-headers linux-pam-dev libpcap-dev openssl-dev utmps-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-openrc"
source="https://download.samba.org/pub/ppp/ppp-$pkgver.tar.gz
	http://distfiles.gentoo.org/distfiles/ppp-dhcpc.tgz
	02_all_make-vars.patch
	03_all_use_internal_logwtmp.patch
	04_all_mpls.patch
	06_all_killaddr-smarter.patch
	08_all_wait-children.patch
	10_all_defaultgateway.patch
	12_all_linkpidfile.patch
	16_all_auth-fail.patch
	18_all_defaultmetric.patch
	19_all_radius_pid_overflow.patch
	20_all_dev-ppp.patch
	21_all_custom_iface_names.patch
	24_all_passwordfd-read-early.patch
	26_all_pppd-usepeerwins.patch
	28_all_connect-errors.patch
	30_all_Makefile.patch
	32_all_pado-timeout.patch
	34_all_lcp-echo-adaptive.patch
	50_all_linux-headers.patch
	51_all_glibc-2.28.patch
	80_all_eaptls-mppe-1.101a.patch
	85_all_dhcp-make-vars.patch
	86_all_dhcp-sys_error_to_strerror.patch
	adelie.patch
	dhcp.patch
	install-path.patch
	musl-fix-headers.patch
	utmpx.patch

	ppp.mod
	ppp.pamd
	pppd.initd
	"

prepare() {
	mv "$srcdir"/dhcp "$builddir"/pppd/plugins
	default_prepare
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make COPTS="$CFLAGS -D_GNU_SOURCE" \
		LIBS="-lutmps -lskarnet -lcrypto -lssl -lpam -lpcap"
	make -C contrib/pppgetpass pppgetpass.vt
}

package() {
	make INSTROOT="$pkgdir" install

	install -Dm 0644 -t "$pkgdir"/usr/include/net/ include/net/ppp_defs.h

	install -d "$pkgdir"/etc/ppp/peers
	install -m 0600 etc.ppp/pap-secrets \
		"$pkgdir"/etc/ppp/pap-secrets.example
	install -m 0600 etc.ppp/chap-secrets \
		"$pkgdir"/etc/ppp/chap-secrets.example
	install -m 0644 etc.ppp/options "$pkgdir"/etc/ppp/options

	install -Dm 0644 "$srcdir"/ppp.pamd "$pkgdir"/etc/pam.d/ppp
	install -Dm 0755 -t "$pkgdir"/usr/bin/ scripts/p{on,off,log}
	install -Dm 0644 -t "$pkgdir"/usr/share/man/man1/ scripts/pon.1

	install -Dm 0755 contrib/pppgetpass/pppgetpass.vt \
		"$pkgdir"/usr/sbin/pppgetpass
	install -Dm 0644 -t "$pkgdir"/usr/share/man/man8/ \
		contrib/pppgetpass/pppgetpass.8

	install -Dm 0644 "$srcdir"/ppp.mod "$pkgdir"/etc/modprobe.d/ppp.conf
	install -Dm 0755 "$srcdir"/pppd.initd "$pkgdir"/etc/init.d/pppd
}

sha512sums="e34ce24020af6a73e7a26c83c4f73a9c83fa455b7b363794dba27bf01f70368be06bff779777843949bd77f4bc9385d6ad455ea48bf8fff4e0d73cc8fef16ae2  ppp-2.4.7.tar.gz
aeaf791b14f5a09c0e2079072a157e65132cbff46e608bc0724e6a5827a01da934f5006e2774eb7105f83e607a52cb4987238f4385cf6f5cc86cbe305a556738  ppp-dhcpc.tgz
8444d7edfe902a83f6cce96d29b9b7fb45ac68bdbe44494797d2a98470b80017489d36feb50cf945cbe72486bac69f45b23790e15cfbd33e07913a857ee80ab7  02_all_make-vars.patch
4c4a5cc6fd8ce3203c41ff07fc0ce5f0468985c779fe05030898d36c404d2086ce7a49336ac58e6502fc2fd14c4de9006028fe19c500d2cac890a16a55c723e8  03_all_use_internal_logwtmp.patch
1d63795949da00a19712aef39a54f338183b6917b246083e04a0b9ee43d346af5adeeb9357cb165587722790fa19b13375d55008646a4e9e2acdf8724bf3c7cc  04_all_mpls.patch
b49086401c1b395ee6caba0418272b2d1b2ab9dcf6f1fc7e7f1280e94fcf17c8fdb884a19f4ef911bd7849a9cceb4cc07fc75438726340cd8c17265d4c2bd4d8  06_all_killaddr-smarter.patch
807707ee9795ef4300b92bacdeba6b9e7332b6ce5a355de3ce77ddcc6dafce069f024131fd3ef282e38be3499e4230ad50fdda2b96e00891f5df1696234b775b  08_all_wait-children.patch
c084237458ceb8704541f6e8424855788dbc2883559c4bf1ff35060e277c2b2ddfadcdb6dedc0bf42a5e83e98cfe7241fae8f6dc59d1ed963ed50356c9fd83ed  10_all_defaultgateway.patch
122b9e3dbc3a9accacb31c653b3c0d368e8cdf3d954a6c93d04ac26ca8f3cb5bfcf8a01881d1cf08e855a1d2d0bd86e7fadba22bb5ada1a86f78e6b9820e6687  12_all_linkpidfile.patch
3a23ef3619b2840eb3f1f7e14bd4765526b09acdfa8ab4d659ad8525a6f96d0cfb7c9fef042cde99ba1de8cf5caa74faa32e2a988453b4b17a70d5cc9a4bcf41  16_all_auth-fail.patch
24b2cf579844bb9e1c0360227a5d35c3510471c0de6f16031d5e192d0ae7b1913aba93c2d99ea5fd3724deb6754f9831c1adb30671a31617268c77c65fc8beaf  18_all_defaultmetric.patch
9fdb3346ef13b250f91f1af55c0efa0f836a60abe9e62fceed30df4e431a3bccdd33b083c2662c45e2091085438ba2221cdc4ae51fc1b05a666d77f74d461214  19_all_radius_pid_overflow.patch
82c80701095a2d9707afbf5fc29bdf2fc3f92252b7de5de1d639f8607096a9d34ce90ffd0a2f770512b590a27dec56f2b03e5e9f4c9e56e1f362a2387d9fb568  20_all_dev-ppp.patch
314e0939b546af5229db34888284a06e07d7b4c94190bf95d4382d3ff39935f18ecc6172f62309e4f63a00fdfceca73d908da8d82c95fd0b926b1832968ee3cc  21_all_custom_iface_names.patch
2508cf1285a086c917ba5deffc96c1c29a511f1c1a0ef94c811b2bf139aed0218f34d7c1f1004890e105c47dffc5834a049dbe7686611d0fc8b8607ccdc86105  24_all_passwordfd-read-early.patch
3eb55fb681e7fecf4e09b53683f509d2db3779599dd60fb89f88cd644c92d779f4720114546ba458262e8239581061e4a450143338c4725ada18b7ca314e12b0  26_all_pppd-usepeerwins.patch
2e0bd81124bcd7c1234089f11e0b607b19047d279dc436ced3a4b8d793bcee4fcececd948b6a0755a02f68542c5c5e30b6f8541f90b998c09da8d50362827520  28_all_connect-errors.patch
e495a489ee98258a3a4549127faca2c41feff27dff296065c2e41bfc696ced2ad1062ea0aa5bf3cc2425c85b4494ebbcbaaabacd8a3ea8ce8fab28acea028336  30_all_Makefile.patch
77c0585b46f4fc090a67198d800d67dab2ce75eadcf2153c6e800e274b53ced6b512fd6eb4767c632f765bacd6c332f8d2a68233abb3781d6c62321d6bbb6052  32_all_pado-timeout.patch
0bd928f45708f10835250fd6a6162f6e148dca172c810511c1f4b1fe56602c4869e7f622e95a3f4a41e5502ddefd0cf6054cd57211bc83426d5da5b5f10dac26  34_all_lcp-echo-adaptive.patch
cda8e347eef7f26589cf1a12243f4d77de34d5736e3cb04fda9063adc0d931ef7ec7dbb2f142f1dfabc6d3ee04a252d2dd67d2c99ad9c01f2bd343bec88abe97  50_all_linux-headers.patch
fc012971a062456fa4e253f5b4a5e2ce56ae1852293d0245ecfd165ba162fa76ec2c28e1035dd89de3e9d43941d528e2d95a40552eb8037a5ba084c1717c20d1  51_all_glibc-2.28.patch
977b247e87557c4c57a871470c40670457e285ca59357dabab854ab16cc5578526ddf880410aa2efc7078e16d32d7afea4b69928af14ac4449523f6a591284f1  80_all_eaptls-mppe-1.101a.patch
2d294bfe455648949cedb47a12a07913f0395aadbe2566c1e90d70fc37baa8635a667ab45195a697567f8d52de88771c499adffee82cde2e9e318ed858b6007b  85_all_dhcp-make-vars.patch
44d5528c057d0abf2b45ba04943a52b6b94209434a06aa565f8a74acdd660efd86fe13280d540383168eaedad9f627673410bb8468860b64adb3145030e12889  86_all_dhcp-sys_error_to_strerror.patch
2ba9ba8856e569c204a0e058a3e7a4a74f331118cb33bbca445a87b54bb0e4f0da2a968df5f394633911603359284831a80a4d9c793d795eef8477d00bab63f1  adelie.patch
6d38f9779945bce2277f2d52d66dd79d2696f02c44186e1750d236f2d77d692746a8e8c164d925d5bb32dbfd02a723cabb59304f05954e0b5f7adada208ee220  dhcp.patch
fb4ae2c2ba4ecdd1c85f6e5f730fd9659cf1fbc7a8a004b09042adafee7e4af6238df8deb3dbd3dc9c69407d9ebc4c82e1792a43b4aaf8ac18ebe18268b50597  install-path.patch
2f071ea9db15e4abf1bed6cce8130dc81b710a31bfef5fa8f9370c353f845dbc47674b1551b8e040478e5156add6f98d480530206125e8bb308f0f4288d1eec6  musl-fix-headers.patch
723ff3dd0aee13f9878559aa433b314af6043523a2bafd5957258809a645942f7d34b5bd659869a1528cf7b1a462ad7cc2dbf18e7986220f5f685f2c1ea1d36b  utmpx.patch
58bf5d6d286a08bd6dd595b39ee425efedd5745dddf33a9c90505891546eb46f4cf1306d83911bef61bc4611816aa0f6aef5d3e0f14c2f4ddd0a588780570041  ppp.mod
e30a397392d467ac3c78f6533f3adff5de7d38f0372d8d6f7b186db4ec69ddf12463d467d8a86eb5867effeb7dd6bd16942a98fb3a3ab59ff754a123e16d0938  ppp.pamd
bd6f43588b037367ffdb57f5e331492dcaa5969003e219c2dc8b90e6be1aa407282ff6114b91d1379ebeff766983fa0622456520cc0ac592b4f0b1496acf21bf  pppd.initd"
