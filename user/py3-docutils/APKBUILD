# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=py3-docutils
_pkgname=docutils
pkgver=0.14
pkgrel=0
pkgdesc="Documentation utilities for Python"
url="https://pypi.python.org/pypi/docutils"
arch="noarch"
# Certified net clean
license="Custom"
depends="python3 py3-pillow py3-roman"
makedepends="python3-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	cd "$builddir"
	python3 setup.py build
}

check() {
	cd "$builddir/test3"
	python3 alltests.py
}

package() {
	cd "$builddir"
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	# Remove .py extension from executable files.
	local path; for path in "$pkgdir"/usr/bin/*.py; do
		mv "$path" "${path%.py}"
	done
}

doc() {
	cd "$builddir"

	local docdir="$subpkgdir/usr/share/doc/$pkgname"
	mkdir -p "$docdir"
	cp -R docs/* "$docdir"
	cp *.txt "$docdir"

	local licdir="$subpkgdir/usr/share/licenses/$pkgname"
	mkdir -p "$licdir"
	rm -f licenses/docutils.conf
	cp licenses/* "$licdir"
}

sha512sums="1ed72c2ef7d2ca38d1c6f3154b7986ea46f599d9bd826405a5257fdd7740c8626957d6298aa95fb0edea8a24515de22f1ad9b2ecbd59341a1ab7a2bab30f500c  py3-docutils-0.14.tar.gz"
