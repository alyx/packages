# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libnfs
pkgver=3.0.0
pkgrel=0
pkgdesc="Client library for accessing NFS shares"
url="https://github.com/sahlberg/libnfs"
arch="all"
license="LGPL-2.1+ AND BSD-2-Clause-FreeBSD AND GPL-3.0+"
makedepends="autoconf automake libtool"
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/sahlberg/libnfs/archive/libnfs-$pkgver.tar.gz
	unconditional-sys-time.h.patch
	"
builddir="$srcdir"/libnfs-libnfs-$pkgver

build() {
	cd "$builddir"
	./bootstrap
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
        make DESTDIR="$pkgdir" install
}

sha512sums="9af31f8824431e9d28267c468dafc7cfc4062b1a280ca141036bc28a2ba544c4470a67955b5e5fbcc6c175435812381013b4c5d3d3d1a175d5efc7b802ae9b3b  libnfs-3.0.0.tar.gz
39ff82b5371185172501babbb9b48c019408c21e3c9f8a17a2d8a70f7c35e648704873db368aceb23901a99f7f44d04495d6754c4dba03b5d2e593381ed16c53  unconditional-sys-time.h.patch"
