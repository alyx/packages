# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=apache-httpd
_pkgreal=httpd
pkgver=2.4.39
pkgrel=0
pkgdesc="Open-source HTTP server"
url="https://httpd.apache.org"
arch="all"
license="Apache-2.0"
depends=""
install="$pkgname.pre-install $pkgname.pre-upgrade"
makedepends="apr-dev apr-util-dev autoconf automake libxml2-dev nghttp2-dev
	openssl-dev pcre-dev sed zlib-dev"
pkgusers="apache"
pkggroups="apache"
subpackages="$pkgname-dev
             $pkgname-doc
	     $pkgname-http2
	     $pkgname-icons::noarch
	     $pkgname-ldap
	     $pkgname-openrc"
provides="apache2 apache2-ssl"
source="http://archive.apache.org/dist/$_pkgreal/$_pkgreal-$pkgver.tar.bz2
	adelie.layout
	apache-httpd.confd
	apache-httpd.initd
	apache-httpd.logrotate
	conf/httpd.conf
	conf/alias.conf
	conf/http2.conf
	conf/host.conf
	conf/proxy.conf
	conf/ssl.conf
	conf/userdir.conf"
builddir="$srcdir/$_pkgreal-$pkgver"
options="suid !check"

# 2.4.30: (unreleased)
# - CVE-2017-15710
# - CVE-2018-1283
# - CVE-2018-1303
# - CVE-2018-1301
# - CVE-2017-15715
# - CVE-2018-1312
# - CVE-2018-1302
# 2.4.34:
# - CVE-2018-8011
# - CVE-2018-1333

prepare() {
	default_prepare
	cat "$srcdir"/adelie.layout >> "$builddir"/config.layout
}

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-layout=Adelie \
		--enable-so \
		--with-mpm=event \
		--enable-mods-shared=reallyall \
		--enable-ssl \
		--disable-lua \
		--enable-suexec \
		--with-suexec-caller=apache \
		--with-suexec-docroot=/srv/localhost \
		--with-suexec-logfile=/var/log/apache-httpd/suexec.log \
		--with-suexec-bin=/usr/sbin/suexec \
		--with-apr=/usr/bin/apr-1-config \
		--with-apr-util=/usr/bin/apu-1-config \
		--with-pcre=/usr
	make
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" -j1 install

	# apache-provided configuration is awful
	rm "$pkgdir"/etc/apache2/*.conf
	rm -r "$pkgdir"/etc/apache2/extra
	rm -r "$pkgdir"/etc/apache2/original
	install -Dm644 "$srcdir"/httpd.conf "$pkgdir"/etc/apache2/httpd.conf
	install -Dm644 "$srcdir"/alias.conf "$pkgdir"/etc/apache2/conf.d/alias.conf
	install -Dm644 "$srcdir"/http2.conf "$pkgdir"/etc/apache2/conf.d/http2.conf
	install -Dm644 "$srcdir"/host.conf "$pkgdir"/etc/apache2/sites.d/localhost.conf
	install -Dm644 "$srcdir"/proxy.conf "$pkgdir"/etc/apache2/conf.d/proxy.conf
	install -Dm644 "$srcdir"/ssl.conf   "$pkgdir"/etc/apache2/conf.d/ssl.conf
	install -Dm644 "$srcdir"/userdir.conf "$pkgdir"/etc/apache2/conf.d/userdir.conf

	# init & logrotate handling
	install -Dm755 "$srcdir"/apache-httpd.initd "$pkgdir"/etc/init.d/apache-httpd
	install -Dm644 "$srcdir"/apache-httpd.confd "$pkgdir"/etc/conf.d/apache-httpd
	install -Dm644 "$srcdir"/apache-httpd.logrotate "$pkgdir"/etc/logrotate.d/apache-httpd

	install -dm2750 -g wheel "$pkgdir"/var/log/apache2
}

dev() {
	default_dev
	depends="$depends perl apr-util-dev"
	install -d "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/apxs "$subpkgdir"/usr/bin
	install -d "$subpkgdir"/usr/share/apache2
	mv "$pkgdir"/usr/share/apache2/build "$subpkgdir"/usr/share/apache2
}

http2() {
	pkgdesc="Highly experimental protocol support for Apache HTTP server"
	local _file
	install -d "$subpkgdir"/etc/apache2/conf.d
	install -d "$subpkgdir"/usr/libexec/apache2

	for _file in etc/apache2/conf.d/http2.conf \
		usr/libexec/apache2/mod_proxy_http2.so \
		usr/libexec/apache2/mod_http2.so; do
		mv "$pkgdir"/$_file "$subpkgdir"/$_file
	done
}

icons() {
	pkgdesc="Public-domain icon pack for Apache HTTP server"
	install -d "$subpkgdir"/usr/share/apache2
	mv "$pkgdir"/usr/share/apache2/icons "$subpkgdir"/usr/share/apache2
}

ldap() {
	pkgdesc="LDAP authentication support for Apache HTTP server"
	install -d "$subpkgdir"/usr/libexec/apache2
	mv "$pkgdir"/usr/libexec/apache2/*_ldap.so \
		"$subpkgdir"/usr/libexec/apache2
}

sha512sums="9742202040b3dc6344b301540f54b2d3f8e36898410d24206a7f8dcecb1bea7d7230fabc7256752724558af249facf64bffe2cf678b8f7cccb64076737abfda7  httpd-2.4.39.tar.bz2
c8bc2bb06ae51b0956e0ee673e80c444551c9b33dfcbb845106477c46d9e52786a8896022e1f00102264fecdf66e35e47fc6cf0abe9836fa536735cff4e6adf4  adelie.layout
336e81fa0d08f8fbe6243d52bd59b12cf2e925deb49b29d7a22953c5d40a951b6b753f51e5a396752cb0bbaf1cf25b1358902f375fb65639d00e62db7ae55ff2  apache-httpd.confd
9893248f1724748c9ef91171728b4c557ced7de543e23ed72cb75c02728048b2813ac99015a4a28e7984daa51faa6e42403c17b875539fe71c33424865eccecf  apache-httpd.initd
18e8859c7d99c4483792a5fd20127873aad8fa396cafbdb6f2c4253451ffe7a1093a3859ce719375e0769739c93704c88897bd087c63e1ef585e26dcc1f5dd9b  apache-httpd.logrotate
cba279f31c12c3516bacf74856493c5a62a94017b3911d457b33e350af646b3e97632b15e396992e5115532c88c81fdd680a951ca0c71eb8a207f39d6c027c41  httpd.conf
83d57c57b809340bb07291c340ded5903ae55c0bba7c48e4eb0d27529ce4ecb6e235378ad770c69c8be2762b5346fa5bec94b6c585510719c37fe6addb33f998  alias.conf
7def0982bda42d5fe3a1812128be455fe0a47161f7fd0facd9ff85056f043a19f520bdb1c01b9f3fb25ddd66aa6224fb4b183f107bccd56719e0c7a88ad70bd9  http2.conf
9745ec511331b2ec399bf7895bc935f9c8526fd81d47cb17e42c1f5c010bbbea0b5ef2f2f4b12984dc8c77f410132010c3e73347a49ebe5feb0c1a763fe73a82  host.conf
905212b9107d71e4d64b338b1c1cf222f7a78979b928e7fa89b06620fbc010df00204a20aa22e69ed49682412d95ecc4e6abaafbb4cb043b31610d5b7a36ac00  proxy.conf
44f97ac0bab5122d409767af388046dfce5cad42ff18d79394f258546d3e578db334da4722cce4dc2ac03f9508a8b97587370cdede5e7678f02b9abc5dda88ee  ssl.conf
dd3c8fa9bc32a92c35df8728f6750fab8dd39be9e90e448a44a77efabc3e5516772607548856af520d4d5e8f2aff11f0b56d29db642e69ab72b1683e6aba5aed  userdir.conf"
