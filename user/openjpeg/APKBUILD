# Contributor: William Pitcock <nenolod@dereferenced.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=openjpeg
pkgver=2.3.1
pkgrel=1
pkgdesc="Open-source implementation of JPEG 2000 image codec"
url="http://www.openjpeg.org/"
arch="all"
options="!check"  # No test suite.
license="BSD-2-Clause-NetBSD"
depends_dev="$pkgname-tools"
makedepends="libpng-dev tiff-dev lcms2-dev doxygen cmake"
subpackages="$pkgname-dev $pkgname-tools"
source="$pkgname-$pkgver.tar.gz::https://github.com/uclouvain/openjpeg/archive/v$pkgver.tar.gz"

build() {
	cmake . \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DOPENJPEG_INSTALL_LIB_DIR=lib \
		-DOPENJPEG_INSTALL_PACKAGE_DIR=lib/cmake/$pkgname-${pkgver%.*}
	make
}

# secfixes:
#   2.3.0-r0:
#     - CVE-2017-14039
#   2.2.0-r2:
#     - CVE-2017-14040
#     - CVE-2017-14041
#     - CVE-2017-14151
#     - CVE-2017-14152
#     - CVE-2017-14164
#   2.2.0-r1:
#     - CVE-2017-12982
#   2.1.2-r1:
#     - CVE-2016-9580
#     - CVE-2016-9581

package() {
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="$pkgdesc (tools)"
	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="339fbc899bddf2393d214df71ed5d6070a3a76b933b1e75576c8a0ae9dfcc4adec40bdc544f599e4b8d0bc173e4e9e7352408497b5b3c9356985605830c26c03  openjpeg-2.3.1.tar.gz"
