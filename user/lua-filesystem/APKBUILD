# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=lua-filesystem
_pkgname=luafilesystem
pkgver=1.7.0.2
_pkgver=${pkgver//./_}
_rockver=${pkgver%.*}-${pkgver##*.}
pkgrel=1
pkgdesc="Filesystem functions for Lua"
url="http://keplerproject.github.io/luafilesystem/"
arch="all"
license="MIT"
depends="lua5.3"
makedepends="lua5.3-dev"
source="$_pkgname-$pkgver.tar.gz::https://github.com/keplerproject/$_pkgname/archive/v$_pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$_pkgver"

build() {
	cd "$builddir"
	make CFLAGS="$CFLAGS $(pkg-config lua --cflags) -fPIC"
}

check() {
	cd "$builddir"
	LUA_CPATH=./src/?.so lua tests/test.lua
}

package() {
	local rockdir="$pkgdir"/usr/lib/luarocks/rocks-5.3/$_pkgname/$_rockver
	cd "$builddir"
	make LUA_LIBDIR="$pkgdir"/$(pkg-config --variable=INSTALL_CMOD lua) install
	mkdir -p "$rockdir"
	echo 'rock_manifest = {}' > "$rockdir"/rock_manifest
}

sha512sums="a1d4d077776e57cd878dbcd21656da141ea3686c587b5420a2b039aeaf086b7e7d05d531ee1cc2bbd7d06660d1315b09593e52143f6711f033ce8eecdc550511  luafilesystem-1.7.0.2.tar.gz"
