# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kemoticons
pkgver=5.54.0
pkgrel=0
pkgdesc="Emoticons to express emotions in KDE"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test requires accelerated X11 environment.
license="LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev karchive-dev kconfig-dev kservice-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen
	graphviz"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kemoticons-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="3b76a6e92cdc0db08179e35c2728f8729c0b1e15d54d17ccec2f265266535f5bf8edc791530d3e65a1ab2b645fd898cb0dba4dade95490f4a73b90c804719886  kemoticons-5.54.0.tar.xz"
