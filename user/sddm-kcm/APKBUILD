# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sddm-kcm
pkgver=5.12.7
pkgrel=0
pkgdesc="KDE configuration applet for SDDM"
url="https://www.kde.org/"
arch="all"
license="GPL-2.0+ AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1+ AND GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev
	qt5-qtdeclarative-dev kcoreaddons-dev ki18n-dev kxmlgui-dev kauth-dev
	kconfigwidgets-dev kio-dev libxcb-dev xcb-util-image-dev libxcursor-dev
	knewstuff-dev"
install_if="systemsettings sddm"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/sddm-kcm-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="29d7b03378a6d5a3d58a0c2b85f6dbe4d0114785456072f16872cd09e7ce355f4e59149e31cb69fe7fbb35678c6fc075c788a091c3b638661a64a856aa4a145b  sddm-kcm-5.12.7.tar.xz"
