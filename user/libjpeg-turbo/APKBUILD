# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libjpeg-turbo
pkgver=2.0.2
pkgrel=0
pkgdesc="Accelerated JPEG compression and decompression library"
url="https://libjpeg-turbo.org/"
arch="all"
license="IJG AND BSD-3-Clause AND Zlib"
depends=""
makedepends=""
subpackages="$pkgname-doc $pkgname-dev $pkgname-utils"
source="https://downloads.sourceforge.net/libjpeg-turbo/libjpeg-turbo-$pkgver.tar.gz"

case "$CTARGET_ARCH" in
pmmx | x86 | x86_64)	makedepends="$makedepends nasm" ;;
esac

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_DEFAULT_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DENABLE_STATIC=OFF \
		-DWITH_JPEG8=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

utils() {
	pkgdesc="Utilities for manipulating JPEG images"
	replaces="jpeg"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="204b6d083e99488c975c75efb08699e4dc1c409556e4dee4f21e3ee67e9c6682eb342f2e5712816b0342c00399fbe6e43fbce30c3d22f30f7ef91db006b3be08  libjpeg-turbo-2.0.2.tar.gz"
