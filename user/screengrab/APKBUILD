# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=screengrab
pkgver=1.101
pkgrel=0
pkgdesc="Cross-platform Qt-based software for taking screenshots"
url="https://github.com/lxqt/screengrab"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qttools-dev
	kwindowsystem-dev libqtxdg-dev>=3.3.1"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/screengrab/releases/download/$pkgver/screengrab-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	mkdir -p build && cd build
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} ..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="$pkgdir" install
}

sha512sums="f253c4015f9c9ae6b53bb4919c0301cc843b0d6f6ab0b8ad88befbd397004aa4838d5900d1af86ed348cbba45fa352ded5192d494bd738c2f2b314e9ad73fb13  screengrab-1.101.tar.xz"
