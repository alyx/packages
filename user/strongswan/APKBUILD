# Contributor: Jesse Young <jlyo@jlyo.org>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Lee Starnes <lee@canned-death.us>
pkgname=strongswan
pkgver=5.7.1
_pkgver=${pkgver//_rc/rc}
pkgrel=0
pkgdesc="IPsec-based VPN solution focused on security and ease of use, supporting IKEv1/IKEv2 and MOBIKE"
url="https://www.strongswan.org/"
arch="all"
pkgusers="ipsec"
pkggroups="ipsec"
license="GPL-2.0 AND RSA-MD5 AND RSA-PKCS11 AND DES"
depends="iproute2"
depends_dev=""
makedepends="$depends_dev linux-headers python3 sqlite-dev openssl-dev curl-dev
	gmp-dev libcap-dev"
install="$pkgname.pre-install"
subpackages="$pkgname-doc $pkgname-dbg $pkgname-openrc"
source="https://download.strongswan.org/$pkgname-$_pkgver.tar.bz2
	0205-ike-Adhere-to-IKE_SA-limit-when-checking-out-by-conf.patch
	1001-charon-add-optional-source-and-remote-overrides-for-.patch
	1002-vici-send-certificates-for-ike-sa-events.patch
	1003-vici-add-support-for-individual-sa-state-changes.patch

	strongswan.initd
	charon.initd
	"
builddir="$srcdir/$pkgname-$_pkgver"

# secfixes:
#   5.7.1-r0:
#     - CVE-2018-17540

build() {
	cd "$builddir"

	# notes about configuration:
	# - try to keep options in ./configure --help order
	# - apk depends on openssl, so we use that
	# - openssl provides ciphers, randomness, etc
	#   -> disable all redundant in-tree copies

	./configure --prefix=/usr \
		--sysconfdir=/etc \
		--libexecdir=/usr/lib \
		--with-ipsecdir=/usr/lib/strongswan \
		--with-capabilities=libcap \
		--with-user=ipsec \
		--with-group=ipsec \
		--enable-curl \
		--disable-ldap \
		--disable-aes \
		--disable-des \
		--disable-rc2 \
		--disable-md5 \
		--disable-sha1 \
		--disable-sha2 \
		--enable-gmp \
		--disable-hmac \
		--disable-mysql \
		--enable-sqlite \
		--enable-eap-sim \
		--enable-eap-sim-file \
		--enable-eap-aka \
		--enable-eap-aka-3gpp2 \
		--enable-eap-simaka-pseudonym \
		--enable-eap-simaka-reauth \
		--enable-eap-identity \
		--enable-eap-md5 \
		--enable-eap-tls \
		--disable-eap-gtc \
		--enable-eap-mschapv2 \
		--enable-eap-radius \
		--enable-xauth-eap \
		--enable-farp \
		--enable-vici \
		--enable-attr-sql \
		--enable-dhcp \
		--enable-openssl \
		--enable-unity \
		--enable-ha \
		--enable-cmd \
		--enable-swanctl \
		--enable-shared \
		--disable-static
	make
}

check() {
	cd "$builddir"
	env TESTS_SUITES_EXCLUDE=printf make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
	install -m755 -D "$srcdir/$pkgname.initd" "$pkgdir/etc/init.d/$pkgname"
	install -m755 -D "$srcdir/charon.initd" "$pkgdir/etc/init.d/charon"
}

sha512sums="43102814434bee7c27a5956be59099cc4ffb9bb5b0d6382ce4c6a80d1d82ed6639f698f5f5544b9ca563554a344638c953525b0e2d39bc6b71b19055c80e07fc  strongswan-5.7.1.tar.bz2
193d845e2751c23d98cdf84134c7803f2e412197669c6d6c1c9974041608d154b85594ed3d9ffb923ca22a4d5926c7f2373787ddc7da47b52019e284a1d13211  0205-ike-Adhere-to-IKE_SA-limit-when-checking-out-by-conf.patch
21db8f153f535ef13cc7c9c011f9b90b8c794e0072bd93fda6a0a56dc00d32d04e186b1a72a87a85613b7e511eed5cb96623abf0721c67dd5c96446db969a185  1001-charon-add-optional-source-and-remote-overrides-for-.patch
f7d98fb99b4855e8bfbb7369292c170536b1987e717feeda71f64ab71b35538e7d462609a773c6a6ed08c8e6ee7a186df12e1ea7d64b9dac0b17d4c7af17dab3  1002-vici-send-certificates-for-ike-sa-events.patch
a4235cd07e17ad3441dc391ded11ee9f4debdffa1e8218809731e73a545ca6fcdc0bb87239d41b1102b0b6719a4d31d43758972d2193ebe298b275285de2ce54  1003-vici-add-support-for-individual-sa-state-changes.patch
8b61e3ffbb39b837733e602ec329e626dc519bf7308d3d4192b497d18f38176789d23ef5afec51f8463ee1ddaf4d74546b965c03184132e217cbc27017e886c9  strongswan.initd
1c44c801f66305c0331f76e580c0d60f1b7d5cd3cc371be55826b06c3899f542664628a912a7fb48626e34d864f72ca5dcd34b2f0d507c4f19c510d0047054c1  charon.initd"
