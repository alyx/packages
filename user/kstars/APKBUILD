# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kstars
pkgver=3.2.3
pkgrel=0
pkgdesc="Desktop planetarium"
url="https://www.kde.org/applications/education/kstars/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtsvg-dev knotifyconfig-dev kauth-dev kconfig-dev kcrash-dev
	kdoctools-dev kwidgetsaddons-dev knewstuff-dev ki18n-dev kio-dev
	kxmlgui-dev kplotting-dev knotifications-dev eigen-dev mesa-dev
	qt5-qtwebsockets-dev libraw-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/kstars/kstars-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DEIGEN3_INCLUDE_DIR=/usr/include/eigen3 \
		${CMAKE_CROSSOPTS} \
		-Bbuild
	make -C build
}

check() {
	make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="29c534c860cb1ab2c52e497061b16c7870e63253a7184f79ed8565d56abbc2909160d7f8c649764e4eb7462630e30e4428a88753e7987e97e530b174076cbd37  kstars-3.2.3.tar.xz"
