# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sonnet
pkgver=5.54.0
pkgrel=0
pkgdesc="Framework for implementing portable spell check functionality"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules aspell-dev hunspell-dev
	qt5-qttools-dev doxygen graphviz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-aspell"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/sonnet-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	# Highlighter test requires X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E sonnet-test_highlighter
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

aspell() {
	pkgdesc="$pkdesc (aspell backend)"
	install_if="$pkgname=$pkgver-$pkgrel aspell"
	mkdir -p "$subpkgdir"/usr/lib/qt5/plugins/kf5/sonnet
	mv "$pkgdir"/usr/lib/qt5/plugins/kf5/sonnet/sonnet_aspell.so \
		"$subpkgdir"/usr/lib/qt5/plugins/kf5/sonnet/sonnet_aspell.so
}

sha512sums="fe7551fb65fe896a872c3f0af4ce1c60f4b974f993465b5ae7b3eea219a0964f7ae5107aa9346884ffd37c95b4bf7ea0bed05592245978c07fd633af88eeca88  sonnet-5.54.0.tar.xz"
