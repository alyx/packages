# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kjs
pkgver=5.54.0
pkgrel=0
pkgdesc="Independent, free JavaScript engine"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+ AND BSD-3-Clause AND MIT"
depends=""
depends_dev="qt5-qtbase-dev pcre-dev libxml2-dev icu-dev"
makedepends="$depends_dev cmake extra-cmake-modules kdoctools-dev perl"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kjs-$pkgver.tar.xz"

build() {
	cd "$builddir"
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS}
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="22ad1133cfe8f5186cfdc517a8bf07eba8ac3f19dea62e2f77d1ba9d34d655d26d195d2929a40180d755efcb7814339fb976090373bdc393fbf5ef6cab8d5d32  kjs-5.54.0.tar.xz"
