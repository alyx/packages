# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ode
pkgver=0.14
pkgrel=0
pkgdesc="High performance library for simulating rigid body dynamics"
url="https://ode.org/"
arch="all"
options="!check"  # Fails 48/52 tests
license="LGPL-2.1+ OR BSD-3-Clause"
depends=""
makedepends="autoconf automake libtool"
subpackages="$pkgname-dev"
source="https://bitbucket.org/odedevs/ode/downloads/ode-$pkgver.tar.gz
	fix-test-link.patch
	"

build() {
	cd "$builddir"

	./bootstrap
	CXXFLAGS="$CXXFLAGS -fpermissive" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-shared \
		--disable-static \
		--disable-double-precision \
		--enable-libccd \
		--enable-ou
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="1f74c3c4687ee9665fa70e65a79100747fba577729830a90806e374115f1c161a2f4545cf591b0979054aa3e2f9a3673635668cb7362ab5c213ada0d39b1a03d  ode-0.14.tar.gz
8630d5d059fd0f623db6af4000666868358002a42ba84817117b1fb5e01c776bb23cbf1c8c43181d7bf40a0d71b640f9d2f9785461d8a77877dcbdadd775792e  fix-test-link.patch"
