# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=glib
pkgver=2.60.4
pkgrel=0
pkgdesc="Common C routines used by Gtk+ and other libs"
url="https://developer.gnome.org/glib/"
arch="all"
options="!check" # Now requires D-Bus running.
license="LGPL-2.1+"
depends=""
depends_dev="perl python3 attr-dev bzip2-dev libffi-dev util-linux-dev"
checkdepends="tzdata shared-mime-info"
makedepends="$depends_dev meson ninja pcre-dev xmlto zlib-dev"
triggers="$pkgname.trigger=/usr/share/glib-2.0/schemas:/usr/lib/gio/modules"
source="https://download.gnome.org/sources/$pkgname/${pkgver%.*}/$pkgname-$pkgver.tar.xz
	0001-gquark-fix-initialization-with-c-constructors.patch
	broken-gio-tests.patch
	fix-spawn.patch
	i386-fpu-test.patch
	musl-no-locale.patch
	ridiculous-strerror-nonconformance.patch
	meson-sucks-and-i-hate-you-so-much-right-now.patch
	"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-static $pkgname-dev $pkgname-lang $pkgname-bash-completion:bashcomp:noarch"

# secfixes:
#   2.60.4-r0:
#     - CVE-2019-12450

build() {
	meson --default-library=both \
		-Dprefix=/usr \
		-Dselinux=disabled \
		-Dman=true build
	ninja -C build
}

check() {
	# workaround if a user builds it on a computer running X11
	DISPLAY= ninja -C build test
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
	rm -rf "$pkgdir"/usr/lib/charset.alias
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/bin "$subpkgdir"/usr/share
		find "$pkgdir"/usr/bin ! -name "glib-compile-schemas" -a \( \
		-name "gdbus-codegen" -o \
		-name "gobject-query" -o \
		-name "gresource" -o \
		-name "gtester*" -o \
		-name "glib-*" \) \
		-exec mv {} "$subpkgdir"/usr/bin \;
	mv "$pkgdir"/usr/share/gdb "$pkgdir"/usr/share/glib-2.0 \
		"$subpkgdir"/usr/share
}

static() {
	pkgdesc="$pkgdesc (static libraries)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir"/usr/lib/
}

bashcomp() {
	pkgdesc="Bash completion for $pkgname"
	depends=
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/bash-completion "$subpkgdir"/usr/share
	[ "$(ls -A "$pkgdir"/usr/share)" ] || rmdir "$pkgdir"/usr/share
}

sha512sums="614d25652ec9e8387f7865777e128b7f6fd68ff4a1a000868117cbcf5210b5f6aa476eb2b795a6dde56b997906aeb2157c83308f1421a27c4e379522d0ed0afc  glib-2.60.4.tar.xz
32e5aca9a315fb985fafa0b4355e4498c1f877fc1f0b58ad4ac261fb9fbced9f026c7756a5f2af7d61ce756b55c8cd02811bb08df397040e93510056f073756b  0001-gquark-fix-initialization-with-c-constructors.patch
657d3fdf7f8ac7edd5bae572d00497f2236fbe5a91a577f3cb7ceb2ccf067f2944918b2b90308fc1061f45daf3d4910067b98642383ce8761da2a1faff6e9b4f  broken-gio-tests.patch
0f0a98784aeed92f33cd9239d2f668bdc6c09b84ed020825ae88f6aacf6a922152dc3e1384c40d9f30f54c5ab78fe17e0ee5c42b268b297b595d2a6cde5b8998  fix-spawn.patch
aa7444bbdf7b88798adc67c15cdb8b7459450c0b7357caea16b74462c5c9179ba80d4018b1e656e90a5e3be5b2e3c14e9b8c0ccbb2ee4d8c92dc8fa627518b84  i386-fpu-test.patch
34129be883011d266074783b7283af5c6a45055b85026b5aab47a3a0dfa1316a0a6502aaf78ab172356f67493ea7c4867b53279c25967a8cc5bee9320dbff96a  musl-no-locale.patch
56c10a0f64cbd8ce584d428f818e7e678fdeb40a32df792843208ddfa3135d362cc2077bc9fe3bfebe13ee6af0ecf6403a593ad727e0a92276074a17a9c7029c  ridiculous-strerror-nonconformance.patch
d788005bc92ed1510235b77a9a26eecd78840a3e915a64e53e1e93cd1efaaeb1b9b5edb115f41352fcbdab10006b91b4aae7af20184c904e869a0d6893080368  meson-sucks-and-i-hate-you-so-much-right-now.patch"
