# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=tiff
pkgver=4.0.10
pkgrel=1
pkgdesc="Library to read, create, and manipulate TIFF image files"
url="http://www.libtiff.org/"
arch="all"
license="MIT"
depends=
depends_dev="zlib-dev libjpeg-turbo-dev"
makedepends="libtool autoconf automake $depends_dev"
subpackages="$pkgname-doc $pkgname-dev $pkgname-tools"
source="http://download.osgeo.org/libtiff/$pkgname-$pkgver.tar.gz
	CVE-2019-6128.patch
	CVE-2019-7663.patch
	"
# secfixes: libtiff
#   4.0.10-r1:
#     - CVE-2019-6128
#     - CVE-2019-7663
#   4.0.9-r1:
#     - CVE-2017-18013
#   4.0.9-r0:
#     - CVE-2017-16231
#     - CVE-2017-16232
#   4.0.8-r1:
#     - CVE-2017-9936
#     - CVE-2017-10688
#   4.0.7-r2:
#     - CVE-2017-7592
#     - CVE-2017-7593
#     - CVE-2017-7594
#     - CVE-2017-7595
#     - CVE-2017-7596
#     - CVE-2017-7598
#     - CVE-2017-7601
#     - CVE-2017-7602
#   4.0.7-r1:
#     - CVE-2017-5225

build() {
	cd "$builddir"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-cxx
	make
}

check() {
	cd "$builddir"
	make check
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="Command-line utility programs for manipulating TIFF files"
	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="d213e5db09fd56b8977b187c5a756f60d6e3e998be172550c2892dbdb4b2a8e8c750202bc863fe27d0d1c577ab9de1710d15e9f6ed665aadbfd857525a81eea8  tiff-4.0.10.tar.gz
8dc336e6c863524e3622f61ec6583eebe13fde55649cd8c812e3f6752242a23ff72cfb680dfcbe47d1503a058f5f9001415ae112220729e4ab50fe81190e327e  CVE-2019-6128.patch
6fb7e9aa0afbae96fd6e78c2401262e496f5d62980ea02712bc43f8749341d030df3625f10413f5ed3e130e88d609c2374ae69807a1f9e54ed91cbd8411aab62  CVE-2019-7663.patch"
